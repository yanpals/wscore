<?php
//GearmanWorker;
//GearmanJob;
//use \PDO;

		function connect()
		{
		// create DB object for wokondb
		$host = 'localhost';
		$dbName = 'wokondb';
		$user = 'root';
		$pwd = 'linuxguy';
		$dsn = 'mysql:host=' . $host . ';dbname=' . $dbName . ';charset=utf8';
		return new PDO($dsn, $user, $pwd);

		}

		function connect2()
		{
		// create DB object for wokon_user_statistics
		$host2 = 'localhost';
		$dbName2 = 'wokon_user_statistics';
		$user2 = 'root';
		$pwd2 = 'linuxguy';
		$dsn2 = 'mysql:host=' . $host2 . ';dbname=' . $dbName2 . ';charset=utf8';
		return  new PDO($dsn2, $user2, $pwd2);
		}



		$worker = new GearmanWorker();
        $worker->addServer();

		$worker->addFunction('commenterSubscriberOncomment', function(GearmanJob $job){//this will subscribe commenter to notifications
		$workload = json_decode($job->workload());
		return commenterSubscriberOncomment($workload->threadId, $workload->postId, $workload->userId, $workload->activityType,
		$workload->commentId);
		});


		$worker->addFunction('postOwnerSubscriberoncomment', function(GearmanJob $job){//subscribes postowner
		$workload = json_decode($job->workload());
   		return postOwnerSubscriberoncomment($workload->threadId2, $workload->postId, $workload->postOwnerUserId, $workload->activityType,
		$workload->commentId);
		});

		$worker->addFunction('interactionLoggerOncomment', function(GearmanJob $job){//logs into user interactions table
		$workload = json_decode($job->workload());

   		return interactionLoggerOncomment($workload->userId, $workload->viewerId, $workload->objectId, $workload->objectTypeId, $workload->privacy,
   			$workload->interactionType, $workload->interactionLevel, $workload->date);
		});

		while ($worker->work());





	 function commenterSubscriberOncomment($threadId, $postId, $userId,  $activityType, $commentId)/*will check if commenter has been subscribed, if not,
	 he/she will be suscribed*/
	 {
	 	$db = connect();
		$count = checkSubscriptionOnComment($userId, $postId);
		if($count == 0){
			$st = $db->prepare("INSERT INTO PostThreadUsers (ThreadId, PostId, UserId, ActivityType, ActivityId)values
			(:ThreadId, :postId, :UserId, :ActivityType, :ActivityId);");
			$st->bindValue(':ThreadId', $threadId, PDO::PARAM_STR);
			$st->bindValue(':UserId', $userId, PDO::PARAM_STR);
			$st->bindValue(':postId', $postId, PDO::PARAM_STR);
			$st->bindValue(':ActivityType', $activityType, PDO::PARAM_INT);
			$st->bindValue(':ActivityId', $commentId, PDO::PARAM_STR);
			return ($st->execute()) ? true : false;
		}
	 }

	 function postOwnerSubscriberoncomment($threadId2, $postId, $postOwnerUserId,  $activityType, $commentId)/*will check if postowner has been , if not,     he/she will be suscribed*/
	 {
		$db = connect();
		$count = checkSubscriptionOnComment($postOwnerUserId, $postId);
		if($count == 0){
			$st = $db->prepare("INSERT INTO PostThreadUsers (ThreadId, PostId, UserId, ActivityType, ActivityId)values
			(:ThreadId, :postId, :UserId, :ActivityType, :ActivityId);");
			$st->bindValue(':ThreadId', $threadId2, PDO::PARAM_STR);
			$st->bindValue(':UserId', $postOwnerUserId, PDO::PARAM_STR);
			$st->bindValue(':postId', $postId, PDO::PARAM_STR);
			$st->bindValue(':ActivityType', $activityType, PDO::PARAM_INT);
			$st->bindValue(':ActivityId', $commentId, PDO::PARAM_STR);
			return ($st->execute()) ? true : false;
		}

	 }

	function interactionLoggerOncomment($userId, $viewerId, $objectId, $objectTypeId, $privacy, $interactionType, $interactionLevel, $date)
	//logs into user interactions table
	{
		$db = connect2();//connect2 connects to wokon_user_statistics database
		$st = $db->prepare("insert into UserInteractionScale (UserId, ViewersId, ObjectId, ObjectTypeId, Privacy, InteractionType, InteractionLevel, Date)
	 	values (:userId,:viewerId, :objectId, :objectTypeId, :privacy, :interactionType, :interactionLevel, :date)");

		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->bindValue(":viewerId", $viewerId, PDO::PARAM_STR);
		$st->bindValue(":objectId", $objectId, PDO::PARAM_STR);
		$st->bindValue(":objectTypeId", $objectTypeId, PDO::PARAM_INT);
		$st->bindValue(":privacy", $privacy, PDO::PARAM_INT);
		$st->bindValue(":interactionType", $interactionType, PDO::PARAM_INT);
		$st->bindValue(":interactionLevel", $interactionLevel, PDO::PARAM_INT);
		$st->bindValue(":date", $date, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}

	 function checkSubscriptionOnComment($userId, $postId)//check whether the user has been subscribed to comment notification
	 {
		 $db = connect();
		$st = $db->prepare("Select count(*) as count from PostThreadUsers Where UserId =:userId and PostId =:postId and ActivityType = 1" );
		$st->bindValue(':userId', $userId,PDO::PARAM_STR );
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$st->fetch();
		return $count;

	 }

?>
