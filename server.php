#!/bin/php -q
<?php

error_reporting(E_ALL);

// Prevent script from timing out
set_time_limit(0);

/* Turn on implicit output flushing so we see what we're getting
 * as it comes in. */
ob_implicit_flush();

// Init
require_once("loader.php");

use Mf\WebSocket\Server;


//REMEMBER: Turn off firewall when using local IP apart from 127.0.0.1

$server = Server::getInstance('127.0.0.1', '7000', array(
	//...config
));

// var_dump($server);

/*
// Register logger
$logger = new Logger(array(
	'interface' => 'file',
	'file' => '/logs/access.log'
));
$server->setLogger($logger);

// Unique ID must be generated everytime server is started once.
// Use Guid::createUUID();
$server->setAcceptKey('258EAFA5-E914-47DA-95CA-C5AB0DC85B11');
*/

// Register chat
//use Mf\Chat\Chat;
//$server->attachObserver(new Chat());

// Register chat
use Mf\MainSocket\MainSocket;
$server->attachObserver(new MainSocket());



//
// $server->attachObserver(new LiveBoard());

// Run
$server->run();
//-f "C:\wamp\www\wsengine\server.php"
