<?php
/**
 *
 */
namespace Mf\WebSocket\Event;
use Mf_Core\Event\Observer as EventObserver;
use Mf\WebSocket\Event\Event;

/**
 *
 */
abstract class Observer extends EventObserver
{
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 *
	 */
	abstract public function onMessage(Event $event);

	/**
	 *
	 */
	abstract public function onConnect(Event $event);

	/**
	 *
	 */
	abstract public function onDisconnect(Event $event);
}