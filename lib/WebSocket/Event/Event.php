<?php
/**
 *
 */
namespace Mf\WebSocket\Event;
use Mf_Core\Event\Event as CoreEvent;

/**
 *
 */
class Event extends CoreEvent
{
	/**
	 *
	 */
	public function __construct($type, $params = array())
	{
		parent::__construct($type, $params);
	}
}