
//
// DIFFERENCES BETWEEN Pub/Sub AND Events/EventListeners
//

An event can be defined as "a significant change in state".
For example, when a consumer purchases a car, the car's state changes from "for sale" to "sold"

# BEGIN NOTE
// Do not use a central event dispatcher approach.
$dispatcher = new EventDispatcher();

// Register a listener for a customer object
$dispatcher->addListener('customer.register', array(new CustomerListener(), 'onRegister'));

# END NOTE:

Subjects and Observers
===================================
Observers subscribe to notification of changes in subjects
Subjects publish their changes to observers


Event                   // Created when event actually occurs
EventTarget             // Object on which event occurs
EventListener           // Object watches and reacts when event occurs on target