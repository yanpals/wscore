<?php
namespace Mf\Mainsocket;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\library\Mainsocket\GeneralFunctions;
use Mf\Mainsocket\library\Mainsocket\Comment;
use Mf\Mainsocket\library\Mainsocket\CommentList;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;





Class CommentDeleter {
	
	protected $event;
	protected $message;
	protected $_UserDb;
	protected $_NotificationDb;
	protected $_PostDb;
	protected $_CommentDb;
	protected $_LoggerDb;
	private $_limit = 6;
	public $returnedData = array();
	
	public function __construct($message, $event)
	{
	
		$this->message = $message;
		$this->event = $event;
		
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_NotificationDb = Registry::getInstance()->get('NotificationDb');
		$this->_PostDb = Registry::getInstance()->get('PostDb');
		$this->_CommentDb = Registry::getInstance()->get('CommentDb');
		$this->prepareData($message, $event);
		
	}
	
	
	public function prepareData($message, $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		
		$postId = $message->postId;
		$userId = $message->userId;
		$commentId = $message->commentId;
		if($this->_CommentDb->trashComment($commentId)){//if the comment is deleted
			$commentCount = $this->_CommentDb->countPostComments($postId);//counts the remaining comments
			
			//now fetch the userpals as objects and store them in an array
				$db = $this->_PostDb;
				$postProperties = $db->getPostById($postId);
				$postOwnerUserId = $postProperties["UserId"];
				$UserPalsId = $this->fetchUserPals($postOwnerUserId);
				
				$msg = array("controller"=>"comment", "action"=>"deletecomment", "commentId"=>$commentId, "count" => $commentCount, "postId"=>$postId, "userId"=>$userId);//build the array to return
				$this->returnedData = array("msg"=>$msg, "usr"=>$UserPalsId);
			
		}
		else{//if comment is not deleted
			
		}
		
	}//ends function
	
	
	
	private function fetchUserPals($userId)
	{
		$pals = $this->_UserDb->getUserPalsSpecial($userId, 5000);//fetchs userid of all the pals
		
		if(is_array($pals) && count($pals) > 0){
		$palsId = array();
		
		foreach($pals as $pal)
		{
			$palsId[] = $pal["UserId"];
			
		}
		return $palsId;
		$palsObj = array();
		
		}
	}
	
	
	private function getUserProperties($onePalId){
		//connect to user database and get UserInfo as Object	
		
		$userInfo = $this->_UserDb->fetchUser($onePalId);
		return $userInfo;
		 
	}
		
	
	public function build()
	{
		return $this->returnedData;
	}
	
	
}//ends class



?>