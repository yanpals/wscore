<?php
namespace Mf\Mainsocket;
use \GearmanClient;
use \GearmanTask;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\library\Mainsocket\GeneralFunctions;
use Mf\Mainsocket\library\Mainsocket\Comment;
use Mf\Mainsocket\library\Mainsocket\CommentList;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;


Class CommentWriter {

	protected $event;
	protected $message;
	protected $_UserDb;
	protected $_NotificationDb;
	protected $_MomentDb;
	protected $_CommentDb;
	protected $_LoggerDb;
	private $_limit = 6;
	public $returnedData = array();
	public $instantNotificationRecievers = array();//these are the people that who are legible to recieve notifications
	protected $_listerners;



	public function __construct()
	{


		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_NotificationDb = Registry::getInstance()->get('NotificationDb');
		$this->_MomentDb = Registry::getInstance()->get('MomentDb');
		$this->_CommentDb = Registry::getInstance()->get('CommentDb');
		$this->_LoggerDb = Registry::getInstance()->get('LoggerDb');

	}


	public function execute($message, $event)
	{
		$startTime = microtime(true);
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$gF = new GeneralFunctions();
		$commentId = $gF->IdGenerator();//generate id
		$momentId = $message->momentId;
		$userId = $message->sender;
		$content = $message->content->body;
		$type = $message->content->media->type;
		$url = $message->content->media->url;
		if($url != null){
				if($type == "image"){//if there is an image file uploaded
					$mediaType = 1;
					$commentId = $gF->CommentIdGenWithPhoto();
					$fileName = $this->_getFileName($url);//get file name from url
					$this->_MomentDb->logMedia($userId, $commentId, NULL, $mediaType, 0, NULL, $commentId, $fileName,time());
				}
				else if($type == "video"){//if a video is uploaded
					$mediaType = 2;
					$commentId = $gF->CommentIdGenWithVideo();
					$fileName = $this->_getFileName($url);//get filename from url
					$this->_MomentDb->logMedia($userId, $commentId, NULL, $mediaType, 0, NULL, $commentId, $fileName,time());
				}
			}




		$content = $gF->sanitizeInput($content);//clean the input to remove unwanted elements
		if($this->_CommentDb->insertComment($commentId,  $userId, $content, time(), $momentId,  1, NULL)){
			$comment = new Comment($this->_CommentDb->getCommentById($commentId), false);
			//send to Notification table
				$this->triggerEvent('onComment', $comment);

				$postProperties = $this->_MomentDb->getPostById($postId);
				$loggerObject = array(
				'userId' => $postProperties["UserId"],
				'viewerId' => $userId,
				'objectId' => $postId,
				'objectTypeId' => 1,
				'privacy' => $postProperties["Privacy"],
				'interactionType' => '2',
				'interactionLevel' =>'',
				'date' => date("Y-m-d")
				);
				$loggerObject = json_encode($loggerObject);
				$threadId = $gF->IdGenerator();
				$threadId2 = $threadId."2nd";
				$postOwnerUserId = $postProperties["UserId"];

				//wrap the data to be used for subscribing commenter in object
				$commenterSubProperties = array("threadId"=>$threadId, "postId"=>$postId, "userId"=>$userId, "activityType"=>1, "commentId"=>$commentId);
				$commenterSubProperties = json_encode($commenterSubProperties);

				//wrap the data to be used for subscribing postowner in object
				$postOwnerSubProperties = array("threadId2"=>$threadId2, "postId"=>$postId, "postOwnerUserId"=>$postOwnerUserId, "activityType"=>1,
				 "commentId"=>$commentId);
				$postOwnerSubProperties = json_encode($postOwnerSubProperties);

				$client = new GearmanClient();/*start gearman to do parallel processes, the processes will log data i user_interaction table for trending,
				will check if commenter is subscribed, if no the commenter will subscribe, check if post ownwer is subscribed, if no, will be subscribed*/
				$client->addServer();

				$client->setCompleteCallback(function(GearmanTask $task, $context) use (&$interactionLogger,
				 &$commenterSubscriber, &$postOwnerSubscriber) {

				switch($context) {

				case 'commenterSubscriberOncomment':
					$commenterSubscriber = $task->data();
					break;
				case 'postOwnerSubscriberoncomment':
					$postOwnerSubscriber = $task->data();
					break;
					case 'interactionLoggerOncomment':
					$interactionLogger = $task->data();
					break;
				}
			});


				$client->addTask('commenterSubscriberOncomment', $commenterSubProperties, 'commenterSubscriberOncomment');
				$client->addTask('postOwnerSubscriberoncomment', $postOwnerSubProperties, 'postOwnerSubscriberoncomment');
				$client->addTask('interactionLoggerOncomment', $loggerObject, 'interactionLoggerOncomment');
				$client->runTasks();


				//arrange the message to be sent
				$msg = $this->_arrangeResponse($postId, $commentId, $userId);
				//fetch user pals
				$UserPalsId = $this->fetchUserPals($postOwnerUserId);//fetch user pals

				//data sent back to controller to be distributed
				$this->returnedData = array("msg"=>$msg, "usr"=>$UserPalsId, "notReciever"=>$this->instantNotificationRecievers,
				 "startTime"=>$startTime);
			 }
		else
		{
			$data = json_encode(array("status"=>"error"));
			$server->send($client->socket, $data);
		}


	}//ends dunction


	private function _getFileName($url)
	{
	$guid = explode("/",$url);
	return  $guid[count($guid)-1];
	}






	public function notifyPostOwner($comment){

		$postDb = $this->_MomentDb;
		$postOwner = $this->_MomentDb->getPostOwner($comment->momentId);

		//check if the commenter is not the Post owner
		if($postOwner != $comment->userId){
			if(!$this->_NotificationDb->checkStoppedSubscription($postOwner, $comment->postId)){//check whether the person has stopped notification
				$this->instantNotificationRecievers[] = $postOwner;//add the post owner to those that will be notified instantly

				$userDb = $this->_UserDb;

				$PostId = $comment->postId;

				$contArray = json_encode(array("PostId"=>$comment->postId, "CauseUserId"=>$comment->userId));//creates an array to hold the content of            	the notification, the postId involved ant the userid of the person that made the notification to take place

				$status  = 0;
				$dateTimeLogged = time();
				$userId = $postOwner;
				$type = 1;
				$notificationArea = 1;//for board post
				$this->_NotificationDb->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);
			}
		}
		return true;

	}



	public function notifyOtherCommenters($comment)
	{
		$commentDb = $this->_CommentDb;
		$postDb = $this->_MomentDb;
		$userDb =  $this->_UserDb;
		//fetch Poster Name using the postId
		$posterName = $userDb->fetchUserName($postDb->getPostOwner($comment->postId));
		$postId = $comment->postId;

		$postOwnerUserId = $postDb->getPostOwner($comment->postId);


		$contArray = json_encode(array("PostId"=>$comment->postId, "CauseUserId"=>$comment->userId));
		//creates an array to hold  the postId involved and 	the userid of the person that made the notification to take place

		$result = $commentDb->getPostCommenters($comment->postId);//fetch Other Commenters for that post

		foreach($result as $res){
			if($comment->userId != $res["UserId"]  && $res["UserId"] != $postOwnerUserId ){
				//this prevents the commenter from being notified and the postowner from recieving double notifications
				$this->instantNotificationRecievers[] = $res["UserId"]; //adds the user to people that will recieve notifications instantly
				$status  = 0;
				$dateTimeLogged = time();
				$userId = $res["UserId"];
				$type = 1;//for comment
				$notificationArea = 1; //for board post
				$this->_NotificationDb->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);

			}//ends first if
		}//ends foreach

	   return true;
	}//ends function


	private function _arrangeResponse($postId, $commentId, $userId)
	{

		$db = $this->_CommentDb;


		$oldercomments = $db->getCommentByPostId($postId, $this->_limit, 0);
		$oldercomments = array_reverse($oldercomments);

		$comments =  new CommentList($oldercomments);
		$commentA = array();
		foreach($comments->get('commentsArray') as $comment){
		   	$commentA[] = $comment;
		}

		$commentCount = $db->countPostComments($postId);
		//return $commentA;

		$list =  $commentA;
		if(count($oldercomments) > 1){

		}
		return array("userId"=>$userId, "postId"=>$postId, "commentId"=>$commentId, "commentCount"=>$commentCount,
		 "commentList"=>$list, "controller"=>"comment", "action"=>"writecomment");

	}//ends arrange url






	private function fetchUserPals($userId)
	{
		$pals = $this->_UserDb->getUserPalsSpecial($userId, 5000);//fetchs userid of all the pals

		if(is_array($pals) && count($pals) > 0){
		$palsId = array();

		foreach($pals as $pal)
		{
			$palsId[] = $pal["UserId"];

		}
		return $palsId;
		$palsObj = array();


		}//ends if
	}


	private function getUserProperties($onePalId){
		//connect to user database and get UserInfo as Object

		$userInfo = $this->_UserDb->fetchUser($onePalId);
		return $userInfo;

	}


	public function AddEventListerner($event, $listener, $listenerAction){

		$this->_listerners[strtolower($event)][] = array($listener, $listenerAction);

	}

	public function triggerEvent($eventName, $eventObject){
		if(isset($this->_listerners[strtolower($eventName)])){
			foreach($this->_listerners[strtolower($eventName)] as $event){
				call_user_func(array($event[0], $event[1]), $eventObject);
			}
		}
	}


	public function build()
	{
		return $this->returnedData;
	}


}//ends class



?>
