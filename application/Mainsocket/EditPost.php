<?php
namespace Mf\Mainsocket;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\library\Mainsocket\GeneralFunctions;


Class EditPost {
	
	protected $event;
	protected $message;
	protected $_PostDb;
	protected $_UserDb;
	public $returnedData = array();
	
	public function __construct($message, $event)
	{
	
		$this->message = $message;
		$this->event = $event;
		$this->_PostDb = Registry::getInstance()->get('PostDb');
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->prepareData($message, $event);
		
	}
	
	
	public function prepareData($message, $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		
		$postId = $message->postId;
		$userId = $message->userId;
		$content = $message->content;
		
		$gF = new GeneralFunctions();
		$content = $gF->sanitizeInput($content);
		$modifiedTime = time();
		
		
				
			if($this->_PostDb->editPost($postId, $content, $modifiedTime)){
				
				//now fetch the userpals as objects and store them in an array
				$userpalsArray = $this->fetchUserPals($userId);
				$msg = array("postId"=>$postId,  "userId"=>$userId,  "controller"=>"post", "action"=>"editpost", "content"=>$content);
				
				
				$UserPalsId = $this->fetchUserPals($userId);
				
				
				$this->returnedData = array("msg"=>$msg, "usr"=>$UserPalsId);
			
		}
		
		
	}//ends dunction
	
	
	
	
	
	
	
	private function fetchUserPals($userId)
	{
		$pals = $this->_UserDb->getUserPalsSpecial($userId, 5000);//fetchs userid of all the pals
		
		if(is_array($pals) && count($pals) > 0){
		$palsId = array();
		
		foreach($pals as $pal)
		{
			$palsId[] = $pal["UserId"];
			
		}
		return $palsId;
		
		}//ends if
	}
	
	
	private function getUserProperties($onePalId){
		//connect to user database and get UserInfo as Object	
		
		$userInfo = $this->_UserDb->fetchUser($onePalId);
		return $userInfo;
		 
	}
		
	
	public function build()
	{
		return $this->returnedData;
	}
	
	
}//ends class



?>