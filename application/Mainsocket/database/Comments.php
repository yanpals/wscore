<?php

use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;




Class DatabaseComments extends Database
{
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
	}


	//this is the constructor method inheriting a construct method from the parent class (Database)

	public function getCommentByPostId($postId, $limit, $offset )//This method fetches all the comments from a particular post using the postid
	  {
			//$query = $this->_driver->prepare("select * from `UserComment` where `PostId` = :PostId and Committed = 1 and Trashed = 0" );
			$query = $this->_driver->prepare('CALL sp_GetCommentsByPostId(:PostId, :limit, :offset)');
			$query->bindValue(':PostId', $postId, PDO::PARAM_STR);
			$query->bindValue(':limit', $limit, PDO::PARAM_INT);
			$query->bindValue(':offset', $offset, PDO::PARAM_INT);

			$query->execute();
			$rows = $query->fetchAll(PDO::FETCH_ASSOC);
			//$query->closeCursor();
			$query = NULL;
			return $rows;
	  }

	//counts the number of comments of a post
	public function countPostComments($postId)
	{
		$que = $this->_driver->prepare('CALL sp_CountPostComments(:PostId)');
		$que->bindValue(':PostId', $postId, PDO::PARAM_STR);
		$que->execute();
		$que->bindColumn('count', $count);
		$que->fetch(PDO::FETCH_ASSOC);
		$que = NULL;
		return $count;
	}

	//this method sends a comment to trash, the user will think it is being deleted.
	public function trashComment($commentId)
	{
		$cqu = $this->_driver->prepare('CALL sp_TrashComment(:CommentId)');
		$cqu->bindValue(':CommentId',$commentId, PDO::PARAM_STR);
		if($cqu->execute()){
			return true;
		}
		else{
			return false;
		}

	}

	public function getCommentById($commentId){
			$query = $this->_driver->prepare("select * from UserComment where CommentId = :commentId");
			//$query = $this->_driver->prepare('CALL sp_GetCommentById(:PostId)');
			$query->bindValue(':commentId', $commentId, PDO::PARAM_STR);
			$query->execute();
			$rows = $query->fetch(PDO::FETCH_ASSOC);
			$query = NULL;
			return $rows;
	}


	public function write($commentId, $postId, $userId, $content, $time, $commited, $photoUrl, $videoUrl){
		$st = $this->_driver->prepare('CALL sp_WriteComment(:commentId, :postId, :userId, :content, :time, :commited, :photoUrl, :videoUrl)');
		//$st = $this->_driver->prepare('insert into UserComment ( CommentId, PostId, UserId, Content, Time) values (:commentId, :postId, :userId, :content, :time, :commited)');
		$bindings = array(':commentId' => $commentId, ':postId' => $postId, ':userId' => $userId, ':content' => $content, ':time' => $time, ':commited' => $commited, ':photoUrl'=> $photoUrl,  ':videoUrl'=> $videoUrl);
		foreach($bindings as $key => $value){
			$st->bindValue($key, $value, PDO::PARAM_STR);
		}
		return ($st->execute()) ? true : false;

	}

	public function getPostCommenters($postId){
		$st = $this->_driver->prepare('CALL sp_GetPostCommenters(:postId)');
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->execute();
		return $st->fetchAll(PDO::FETCH_ASSOC);
	}

	public function update($commentId, $content, $time){
		$st = $this->_driver->prepare('CALL sp_UpdateComment(:commentId, :content, :time)');
		$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
		$st->bindValue(':content', $content, PDO::PARAM_STR);
		$st->bindValue(':time', $time, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}

	 public function editComment($commentId, $content, $Modifiedtime){
		$st = $this->_driver->prepare('CALL sp_EditComment(:commentId, :content, :modifiedTime)');
		$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
		$st->bindValue(':content', $content, PDO::PARAM_STR);
		$st->bindValue(':modifiedTime', $Modifiedtime, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}

	public function subscribePostThreadUsers($threadId, $postId, $userId, $activityType, $activityId)
	{
		$st = $this->_driver->prepare('CALL sp_SubscribePostThreadUsers(:ThreadId, :postId, :UserId,  :ActivityType, :ActivityId)');
		//$st = $this->_driver->prepare("insert into postthreadusers (ThreadId,  PostId, UserId, ActivityType, ActivityId) values(:ThreadId, :UserId, :postId,:ActivityType , :ActivityId )");
		$st->bindValue(':ThreadId', $threadId, PDO::PARAM_STR);
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->bindValue(':ActivityType', $activityType, PDO::PARAM_INT);
		$st->bindValue(':ActivityId', $activityId, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}



	public function getOlderComments($postId, $lastCommentTime, $limit)
	{
		//$st = 	$this->_driver->prepare("call sp_GetOlderComments(:postId, :lastCommentTime, :limit)");
		$st = 	$this->_driver->prepare("select * from usercomment  where PostId =:postId and Time < :lastCommentTime order by Time desc limit :limit");
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->bindValue(':lastCommentTime', $lastCommentTime, PDO::PARAM_STR);
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);
		$st->execute();
		$rows = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $rows;

	}

	public function getRecentComments($postId, $lastCommentTime, $limit)
	{
		//$st = 	$this->_driver->prepare("call sp_GetRecentComments(:postId, :lastCommentTime, :limit)");
		$st = 	$this->_driver->prepare("select * from usercomment  where PostId =:postId and Time > :lastCommentTime order by Time desc limit :limit");
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->bindValue(':lastCommentTime', $lastCommentTime, PDO::PARAM_STR);
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);
		$st->execute();
		$rows = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $rows;

	}

	public function checkCommentIdExistence($commentId)
	{
		$st = 	$this->_driver->prepare("select count(*) as count  from usercomment  where CommentId =:commentId");
		$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn("count", $count);
		$st->fetch();
		if($count == 1){ return true;} else { return false; }

	}


	public function insertComment($commentId,  $userId, $content, $time,  $typeId, $type, $mediaId)
	{
		$st = 	$this->_driver->prepare("INSERT INTO UserComment (CommentId, UserId, Content, Time, TypeId, Type, MediaId) values
		(:commentId, :userId, :content, :time, :typeId, :type, :mediaId) ");
		$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':content', $content, PDO::PARAM_STR);
		$st->bindValue(':time', $time, PDO::PARAM_STR);
		$st->bindValue(':typeId', $typeId, PDO::PARAM_STR);
		$st->bindValue(':type', $type, PDO::PARAM_INT);
		$st->bindValue(':mediaId', $mediaId, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}



}//ends class
