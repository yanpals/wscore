<?php


use \Mf_Core\Registry;
use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;



class DatabaseMoments extends Database{



	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}


  }


  public function logMoment($momentId, $userId , $content, $time, $privacy, $OriginalMomentId, $type)
	{
		$st = $this->_driver->prepare("insert into Moments(MomentId, UserId, Content, Time, Privacy, OriginalMomentId, Type)
		values (:MomentId, :UserId,:Content, :Time, :Privacy, :OriginalMomentId, :type)");

		 $st->bindValue(':MomentId', $momentId, PDO::PARAM_STR);
		 $st->bindValue(':UserId', $userId, PDO::PARAM_STR);
		 $st->bindValue(':Content', $content, PDO::PARAM_STR);
		 $st->bindValue(':Time', $time, PDO::PARAM_STR);
		 $st->bindValue(':Privacy', $privacy, PDO::PARAM_INT);
		 $st->bindValue(':OriginalMomentId', $OriginalMomentId, PDO::PARAM_STR);
     $st->bindValue(':type', $type, PDO::PARAM_INT);
		 return ($st->execute()) ? true : false;
	}

	public function logMedia($userId, $mediaId, $poster, $type, $cropped, $source, $sourceId, $fileName, $time)
	{
		$st = $this->_driver->prepare("insert into Media (MediaId, UserId,  Type,  MediaName,  Source, Poster, Cropped ,SourceId, Time)
		values (:MediaId,:UserId, :Type, :MediaName, :Source, :Poster, :Cropped, :SourceId, :Time)");

		 $st->bindValue(':UserId', $userId, PDO::PARAM_STR);
		 $st->bindValue(':MediaId', $mediaId, PDO::PARAM_STR);
		 $st->bindValue(':Poster', $poster, PDO::PARAM_STR);
		 $st->bindValue(':Type', $type, PDO::PARAM_INT);
		 $st->bindValue(':Cropped', $cropped, PDO::PARAM_INT);
		 $st->bindValue(':Source', $source, PDO::PARAM_INT);
		 $st->bindValue(':SourceId', $sourceId, PDO::PARAM_STR);
		 $st->bindValue(':MediaName', $fileName, PDO::PARAM_STR);
		 $st->bindValue(':Time', $time, PDO::PARAM_STR);
		 return ($st->execute()) ? true : false;
	}


  public function logEligiblePalsToViewPost($momentId, $userId, $UserPalCategory)
  {
    $st = $this->_driver->prepare("insert into MomentUsers (MomentId, UserId, UserPalCategory)	values (:MomentId, :UserId, :UserPalCategory)");
    $st->bindValue(':MomentId', $momentId, PDO::PARAM_STR);
    $st->bindValue(':UserId', $userId, PDO::PARAM_STR);
    $st->bindValue(':UserPalCategory', $UserPalCategory, PDO::PARAM_INT);
    return ($st->execute()) ? true : false;
  }

  public function getPalsINCategory($id)
  {
      $st = $this->_driver->prepare("select  PalUserId from PalCategory WHERE UserPalCategoryId =:id");
      $st->bindValue(':id', $id, PDO::PARAM_INT);
      $st->execute();
      return $st->fetchAll(PDO::FETCH_ASSOC);
  }

  public function checkIfLoggedInMomentUsers($momentId, $userId)
  {
      $st = $this->_driver->prepare("SELECT count(*) as  count from MomentUsers WHERE MomentId =:MomentId and  UserId =:UserId");
      $st->bindValue(':MomentId', $momentId, PDO::PARAM_STR);
      $st->bindValue(':UserId', $userId, PDO::PARAM_STR);
      $st->execute();
  		$st->bindColumn('count', $count);
  		$result = $st->fetch(PDO::FETCH_ASSOC);
      return ($count > 0)? true : false;
  }

  public function fetchUserMoments($userId, $limit)
  {
    $st = $this->_driver->prepare("SELECT * from Moments where (UserId =:userId or UserId in(select UserId from UserPals where
      PalUserId = :userId and   FriendStatus = 1 ) or UserId in (select PalUserId from UserPals where
        UserId = :userId and FriendStatus = 1)) and
     (MomentId not in(select MomentId from UserHidePost where UserId = :userId))  and (Trashed = 0) and
      (Privacy in(1,2))
      union all
      select * from Moments where privacy = 3 and (MomentId  IN (select  MomentId from MomentUsers where UserId =:userId)) and
      (MomentId not in(select MomentId from UserHidePost where UserId = :userId))  and (Trashed = 0)
      order by Time desc LIMIT :limit ");
      $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  		$st->bindValue(':limit', $limit, PDO::PARAM_INT);
      $st->execute();
  		return $st->fetchAll(PDO::FETCH_ASSOC);

  }




  public function countRebroadcast($momentId)
  {
    $st = $this->_driver->prepare("SELECT count(*) as count  from Moments where  OriginalMomentId=:momentId");
    $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
    $st->execute();
    $st->bindColumn('count', $count);
    $result = $st->fetch(PDO::FETCH_ASSOC);
    return $count;
  }


  public function getMediaByMomentId($momentId)
  {
    $st = $this->_driver->prepare("SELECT * FROM Media where SourceId = :sourceId");
    $st->bindValue(':sourceId', $momentId, PDO::PARAM_STR);
    $st->execute();
    return $st->fetchAll(PDO::FETCH_ASSOC);
  }


public function getMomentByMomentId($momentId)
{
  $st = $this->_driver->prepare("select * from Moments where MomentId = :momentId");
  $st->bindParam(':momentId', $momentId, PDO::PARAM_STR);
  return $st->execute() ? $st->fetch(PDO::FETCH_ASSOC) : null;
}


/**
* Checks whether a user has stopped notification for a particular moment
*/
public function stoppedNotification($momentOwnerUserId, $originalMomentId)
{
  $st = $this->_driver->prepare("select count(*) as count from NotificationSubscribers where UserId =:userId and
  Status = 0  and TypeId =:typeId");
  $st->bindValue(':typeId', $originalMomentId, PDO::PARAM_STR);
  $st->bindValue(':userId', $momentOwnerUserId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
  $result = $st->fetch(PDO::FETCH_ASSOC);
  return ($count > 0)? true : false;
}


public function fetchRebroadcasters($momentId, $limit)
{
  $st = $this->_driver->prepare("SELECT  Max(Time) as Time, UserId from Moments where OriginalMomentId =:momentId
   Group by UserId order by Time desc lIMIT $limit");
  $st->bindParam(':momentId', $momentId, PDO::PARAM_STR);
  $st->execute();
  $row = $st->fetchAll(PDO::FETCH_ASSOC);
  //var_dump($row); die;
  return $row;
}


public function fetchPreviousRebroadcasters($momentId, $limit, $pageEnd)
{
  $st = $this->_driver->prepare("SELECT  Max(Time) as Time, UserId from Moments where OriginalMomentId =:momentId and Time > :time
   Group by UserId order by Time desc lIMIT $limit");
  $st->bindParam(':momentId', $momentId, PDO::PARAM_STR);
  $st->bindParam(':time', $pageEnd, PDO::PARAM_STR);
  $st->execute();
  $row = $st->fetchAll(PDO::FETCH_ASSOC);
  //var_dump($row); die;
  return $row;
}


public function checkIfRebroadcasted($momentId, $userId)
{
  $st = $this->_driver->prepare("select count(*) as count from  Moments where UserId =:userId and
  OriginalMomentId =:momentId");
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
  $result = $st->fetch(PDO::FETCH_ASSOC);
  return ($count > 0)? true : false;
}

public function getVideosByMommentId($momentId)
{
  $st = $this->_driver->prepare("select * from  Media where SourceId =:momentId");
  $st->bindParam(':momentId', $momentId, PDO::PARAM_STR);
  $st->execute();
  return $st->fetchAll(PDO::FETCH_ASSOC);
}

public function checkIfNotificationEnabled($momentId, $userId)
{
  $st = $this->_driver->prepare("select count(*) as count  from NotificationSubscribers Where UserId =:userId and
   TypeId =:typeId and Status = 1");
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':typeId', $momentId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
  $result = $st->fetch(PDO::FETCH_ASSOC);
  return ($count > 0)? true : false;
}

public function subscribeForNotification($notificationsubcriberId, $userId, $typeId, $type,  $status)
{
  $st = $this->_driver->prepare("INSERT INTO NotificationSubscribers(NotificationSubscriberId, UserId, TypeId, Type, Status)
   VALUES (:notificationSubcriberId,  :userId, :typeId, :type, :status)");

  $st->bindValue(':notificationSubcriberId', $notificationsubcriberId, PDO::PARAM_STR);
  $st->bindValue(':type', $type, PDO::PARAM_INT);
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':typeId', $typeId, PDO::PARAM_STR);
  $st->bindValue(':status', $status, PDO::PARAM_INT);
if( $st->execute()){ return true;} else{ return false; }
}

public function  deleteFormerRecordsIfExist($userId, $typeId)
{
  $st = $this->_driver->prepare("Delete From NotificationSubscribers Where  UserId =:userId and  TypeId =:typeId");
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':typeId', $typeId, PDO::PARAM_STR);
  $st->execute();
}

public function unsubscribeNotification($userId, $typeId)
{
  $st = $this->_driver->prepare("Update NotificationSubscribers  SET Status = 0 Where  UserId =:userId and  TypeId =:typeId");
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':typeId', $typeId, PDO::PARAM_STR);
  if( $st->execute()){ return true;} else{ return false; }
}

public function favouriteMoment($userId, $momentId, $time)
{
  $st = $this->_driver->prepare("INSERT INTO FavouriteMoments (UserId, MomentId, Date) VALUES (:userId, :momentId, :time)");
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
  $st->bindValue(':time', $time, PDO::PARAM_STR);
  if( $st->execute()){ return true;} else{ return false; }
}

public function DeleteFromFavourite($userId, $momentId)
{
  $st = $this->_driver->prepare("Delete from  FavouriteMoments Where UserId =:userId and MomentId =:momentId");
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);

  if( $st->execute()){ return true;} else{ return false; }
}


public function checkIfFavourited($userId, $momentId)
{
  $st = $this->_driver->prepare("select count(*) as count  from  FavouriteMoments Where UserId =:userId and MomentId =:momentId");
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
  $result = $st->fetch(PDO::FETCH_ASSOC);
  return ($count > 0)? true : false;
}

public function editMoment($momentId, $text)
{
  $st = $this->_driver->prepare("UPDATE  Moments SET  Content =:content WHERE  MomentId =:momentId");
  $st->bindValue(':content', $text, PDO::PARAM_STR);
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
  if( $st->execute()){ return true;} else{ return false; }
}












}
