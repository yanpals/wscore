<?php
/*
 *
 *This class fetches user's pals from  the database
 *
*/


Class DatabasePals extends Database
{
	
	//fetches  a user's pals from  table
	public function fetchPals($userId, $limit, $offset)
	{
		$query = $this->_driver->prepare('CALL sp_FetchPals(:UserId, :limit, :offset)');
		$query->bindValue(':UserId',$userId,PDO::PARAM_STR);
		$query->bindValue(':limit',$limit,PDO::PARAM_INT);
		$query->bindValue(':offset',$offset,PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
		
	}
	
	
	
	
	
	
	public function countUserPals($userId)
	{
		
		$st = $this->_driver->prepare("CALL sp_countUserPals(:userId)");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$result = $st->fetch(PDO::FETCH_ASSOC);
		$st = NULL;
		return $count;
	
	}
	
	
	
	
	
}