<?php
/*
 *
 *This class fetches user's data from  the database
 *
*/
// Direct access check
defined('_PUBLIC') || exit;

use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;



Class DatabaseUser extends Database
{
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
	}

	//fetches  a user's data from userbiodata table
	public function fetchUser($userId)
	{
		$query = $this->_driver->prepare('CALL sp_FetchUserDetailsByUserId(:UserId)');
		//$query = $this->_driver->prepare('select * from `UserBiodata` where `UserId` = :userId');
		$query->bindValue(':UserId', $userId, PDO::PARAM_STR);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_ASSOC);
		$query = NULL;
		return $row;

	}
	
	public function fetchUserName($userId)
	{
		$st = $this->_driver->prepare('CALL sp_FetchUserName(:userId)');
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('Name', $name);
		$st->fetch(PDO::FETCH_ASSOC);
		$st = NULL;
		return $name;	
	}
	
	public function disconnectUsers($userId1, $userId2)
	{
		$st = $this->_driver->prepare('CALL sp_DisconnectUsers(:userId1, :userId2)');	
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}
	
	public function blockUser($userId1, $userId2)//update the userpal table by setting friendstatus to 2
	{
		$st = $this->_driver->prepare('CALL sp_BlockUser(:userId1, :userId2)');	
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}
	
	
	public function checkPalRequest($userId1, $userId2)//checks whether pal request has been sent
	{
		//$st = $this->_driver->prepare('CALL sp_CheckPalRequest(:userId1, :userId2)');	
		$st = $this->_driver->prepare('select count(*) as count from PalRequest where (SenderId =:userId1 and RecieverId =:userId2) or (SenderId =:userId2 and RecieverId =:userId1) ');	
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		$st->execute() ;
		$st->bindColumn('count', $count);
		$st->fetch();
		return $count;
	}
	
	
	public function checkIfPals($userId1, $userId2)//checks if two users are pals
	{
		//$st = $this->_driver->prepare('CALL sp_CheckIfPals(:userId1, :userId2)');	
		$st = $this->_driver->prepare('select count(*) as count from UserPals where (UserId =:userId1 and PalUserId =:userId2) or (UserId =:userId2 and PalUserId =:userId1) ');	
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		$st->execute() ;
		$st->bindColumn('count', $count);
		$st->fetch();
		return $count;
	}
	
	
	
	
	public function sendPalRequest($userId1, $userId2, $friendStatus, $notified, $time, $seen)//sends pal request by inserting into palrequest table
	{
		//$st = $this->_driver->prepare('CALL sp_SendPalRequest(:userId1, :userId2, :friendStatus, :notified, :time, :seen)');	
		$st = $this->_driver->prepare("Insert into PalRequest (SenderId, RecieverId, FriendStatus, Notified, Time, Seen) values (:userId1, :userId2,:friendStatus, :notified, :time, :seen)");	
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		$st->bindValue(':friendStatus', $friendStatus, PDO::PARAM_INT);
		$st->bindValue(':notified', $notified, PDO::PARAM_INT);
		$st->bindValue(':time', $time, PDO::PARAM_STR);
		$st->bindValue(':seen', $seen, PDO::PARAM_INT);
		return ($st->execute()) ? true : false;
	}
	
	
	public function acceptPalRequest($userId1, $userId2, $addedDate,  $friendStatus)
	{
		//$st = $this->_driver->prepare('CALL sp_AcceptPalRequest(:id, :userId1, :userId2, :addedDate, :friendStatus)');	
		$st = $this->_driver->prepare("Insert into UserPals (UserId, PalUserId, AddedDate, FriendStatus) values (:userId1, :userId2,:addedDate, :friendStatus)");
			
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		$st->bindValue(':addedDate', $addedDate, PDO::PARAM_STR);
		$st->bindValue(':friendStatus', $friendStatus, PDO::PARAM_INT);
		return ($st->execute()) ? true : false;
	}
	
	
	public function updatePalRequestStatus($userId1, $userId2, $friendStatus)//updates the palrequest table by changing friend status to 1
	{
		//$st = $this->_driver->prepare('CALL sp_UpdatePalRequestStatus(:userId1, :userId2, :friendStatus)');	
		$st = $this->_driver->prepare('update PalRequest set FriendStatus =:friendStatus where (SenderId =:userId1 and RecieverId =:userId2) or (SenderId =:userId2 and RecieverId =:userId1) ');
		
		
		$st->bindValue(':userId1', $userId1, PDO::PARAM_STR);
		$st->bindValue(':userId2', $userId2, PDO::PARAM_STR);
		$st->bindValue(':friendStatus', $friendStatus, PDO::PARAM_INT);
		return ($st->execute()) ? true : false;
	}
	
	
	public function fetchUserPalRequest($userId, $limit){//fetches user pal request sent from others pals to the logged in user on first call
		//$st = $this->_driver->prepare('CALL sp_FetchUserPalRequest(:UserId, :limit)');
		$st = $this->_driver->prepare('select SenderId, PalRequestId, Time, Seen, UnRead, Notified from PalRequest where RecieverId = :UserId and FriendStatus = 0 order by Time desc  limit :limit');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);	
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;
		
	}
	
	
	public function fetchUserLastObjects($userId)
	{
		//$st = $this->_driver->prepare('CALL sp_FetchUserLastObjects( :UserId)');
		$st = $this->_driver->prepare('select * from lastseennotification where UserId = :UserId');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		if($row == ""){
		$row = null;	
		}
		return $row;
		
	}
	
	public function fetchMorePalRequest($userId, $boffset, $limit)//fetches more pals request sent from others pals to the logged in user when the loggedin  user scrolls down to see more
	{
	//$st = $this->_driver->prepare('CALL sp_FetchMorePalRequest(:UserId,:Boffset :Limit)');
		$st = $this->_driver->prepare('select SenderId, PalRequestId, Time, Seen, Notified  from PalRequest where RecieverId = :UserId and FriendStatus = 0 and Time < :Boffset order by Time desc  Limit :Limit');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':Boffset', $boffset, PDO::PARAM_INT);	
		$st->bindValue(':Limit', $limit, PDO::PARAM_INT);	
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;	
	}
	
	public function countPalRequest($userId)//counts pal request a user has
	{
		//$st = $this->_driver->prepare('CALL sp_CountPalRequest(:UserId)');
		$st = $this->_driver->prepare('select count(*) as count from PalRequest where RecieverId = :UserId and FriendStatus = 0');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->execute();
		$st->bindColumn('count', $count);
		$row = $st->fetchAll();
		return $count;
	}
	
	
	public function countUnseenPalRequest($userId)//counts pal request a user has
	{
		//$st = $this->_driver->prepare('CALL sp_CountUnseenPalRequest(:UserId)');
		$st = $this->_driver->prepare('select count(*) as count from PalRequest where RecieverId = :UserId and FriendStatus = 0 and  Seen = 0');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->execute();
		$st->bindColumn('count', $count);
		$row = $st->fetchAll();
		return $count;
	}
	
	
	public function fetchRecentPalRequest($userId, $toffset, $limit)//fetch the most recent pal request sent from others pals to the logged in user when the logged in user scrolls up
	{
	//$st = $this->_driver->prepare('CALL sp_FetchRecentPalRequest(:UserId,:Toffset :Limit)');
		$st = $this->_driver->prepare('select SenderId, PalRequestId, Time, Seen, UnRead, Notified from PalRequest where RecieverId = :UserId and FriendStatus = 0 and Time > :Toffset order by Time desc  Limit :Limit');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':Toffset', $toffset, PDO::PARAM_INT);	
		$st->bindValue(':Limit', $limit, PDO::PARAM_INT);	
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;		
	}
	
	public function declinePalRequest($userId1, $userId2)//rejects pal request by deleting the row with users. userid2 is the logged in person 
	{	//$t = $this->_driver->prepare('CALL DeclinePalRequest($userId1, $userId2)');
		$st = $st = $this->_driver->prepare('delete from PalRequest where SenderId =:UserId1 and RecieverId =:UserId2');
		$st->bindValue(':UserId1', $userId1, PDO::PARAM_STR);	
		$st->bindValue(':UserId2', $userId2, PDO::PARAM_STR);
		$result = ($st->execute())? true : false;
		return $result;
	}
	
	public function cancelPalRequest($userId1, $userId2)//cancel pal request by deleting the row with users. userid 2 is the logged in person
	{	
		$st = $st = $this->_driver->prepare('delete from PalRequest where SenderId =:UserId2 and RecieverId =:UserId1');
		$st->bindValue(':UserId1', $userId1, PDO::PARAM_STR);	
		$st->bindValue(':UserId2', $userId2, PDO::PARAM_STR);
		$result = ($st->execute())? true : false;
		return $result;
	}
	
	public function countUserPals($userId)//counts the number of pals a user has
	{
		//$st  = $this->_driver->prepare('CALL CountUserpals(:UserId)');
		$st  = $this->_driver->prepare('select count(*) as count from UserPals where (UserId =:UserId)
		 or (PalUserId =:UserId)');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$result = $st->execute();
		$st->bindColumn('count', $count);
		$st->fetch();
		return $count;
	}
	
	
	public function fetchUserPalRequestSent($userId, $limit){//fetches user pal request sent from others pals to the logged in user on first call
		//$st = $this->_driver->prepare('CALL sp_FetchUserPalRequestSent(:UserId, :limit)');
		$st = $this->_driver->prepare('select  RecieverId, PalRequestId, Time, Seen, UnRead, Notified from PalRequest where  SenderId = :UserId and FriendStatus = 0 order by Time desc  limit :limit');
		$st->bindValue(':UserId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);	
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;
		
	}
	
	
	public function fetchPals($userId, $limit, $offset)//fetch all the userid of all pals
	{
		$query = $this->_driver->prepare('CALL sp_FetchPals(:UserId, :limit, :offset)');
		$query->bindValue(':UserId',$userId,PDO::PARAM_STR);
		$query->bindValue(':limit',$limit,PDO::PARAM_INT);
		$query->bindValue(':offset',$offset,PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
		
	}
	
	public function updateAllToSeen($userId)//update the palrequest table to show the the user has seen all palrequest
	{
		$query = $this->_driver->prepare('Update PalRequest Set Seen = 1 where RecieverId = :userId');	
		$query->bindValue(':userId',$userId,PDO::PARAM_STR);
		if($query->execute()){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public function MarkAsReadPalRequest($id)//updates palrequest table to show that the user has clicked the palrequest link
	{
		$query = $this->_driver->prepare('Update PalRequest Set UnRead = 1 where PalRequestId = :id');	
		$query->bindValue(':id',$id,PDO::PARAM_STR);
		if($query->execute()){
			return true;
		}
		else{
			return false;
		}	
	}
	
	
	public function updateAvatarField($userId, $avatarField)//updates the avatar field in the userbiodata table
	{	
		//$st = $this->_driver->prepare('CALL sp_UpdateAvatarField(:userId, :avatar)');
		$st = $this->_driver->prepare("Update UserBiodata set Avatar =:avatar where UserId =:userId");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':avatar', $avatarField, PDO::PARAM_STR);	
		if($st->execute()){
			return true;
		}
		else{
			return false;
		}	
	}
	
	public function fetchUserIdByUsername($urlName)//metheod to get usersId when given the urlname
	{
		//$st = $this->_driver->prepare('CALL sp_FetchUserIdByUsername(:Username)');
		$st = $this->_driver->prepare("SELECT UserId from UserAuthentication where UrlName =:urlName");	
		$st->bindValue(':urlName', $urlName, PDO::PARAM_STR);	
		$st->execute();
		$row = $st->fetch();
		return $row["UserId"];
	}
	
	public function fetchUserLatestEducation($userId)//becarefull, it fetches only one
	{	
		//$st = $this->_driver->prepare('CALL sp_FetchUserLatestEducation(:userId)');
		$st = $this->_driver->prepare("SELECT *  from UserEducation  where UserId =:userId ORDER BY Type desc  limit 1");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);		
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		return $row;
	}
	
	public function fetchUserLatestWork($userId)//becarefull, it fetches only one
	{	
		//$st = $this->_driver->prepare('CALL sp_FetchUserLatestWork(:userId)');
		$st = $this->_driver->prepare("SELECT *  from UserWork  where UserId =:userId ORDER BY BeginYear desc  limit 1");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);		
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		return $row;
	}
	
	public function getUserPalsSpecial($userId, $limit)//fetches user pals  when requested for the first time
	{	
		//$st = $this->_driver->prepare('CALL sp_GetUserPalsSpecial(:userId, :limit)');
		$st = $this->_driver->prepare("SELECT Id,PalUserId as UserId FROM UserPals where UserId = :userId union all Select Id, UserId as UserId from 
		UserPals where PalUserId = :userId ORDER BY Id desc limit :limit");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);		
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;
	}
	
	
	public function loadMorePals($userId, $limit, $boffset)//fetches user pals  when user scrolls to see down
	{	
		//$st = $this->_driver->prepare('CALL sp_LoadMorePals(:userId, :limit, :boffset)');
		$st = $this->_driver->prepare("SELECT Id,PalUserId as UserId FROM UserPals where UserId = :userId  and (Id < :boffset)
		 union all Select Id, UserId as UserId from UserPals where PalUserId = :userId and (Id < :boffset) ORDER BY Id desc limit :limit");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);
		$st->bindValue(':boffset', $boffset, PDO::PARAM_INT);		
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;
	}
	
	
	public function loadRecentPals($userId, $limit, $toffset)//fetches user pals when user scrolls to see top 
	{	
		
		//$st = $this->_driver->prepare('CALL sp_LoadRecentPals(:userId, :limit, :toffset)');
		$st = $this->_driver->prepare("SELECT Id,PalUserId as UserId FROM UserPals where UserId = :userId  and (Id > :toffset)
		 union all Select Id, UserId as UserId from UserPals where PalUserId = :userId and (Id > :toffset) ORDER BY Id desc limit :limit");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->bindValue(':limit', $limit, PDO::PARAM_INT);
		$st->bindValue(':toffset', $toffset, PDO::PARAM_INT);		
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;
	}
	
	
	public function countUserPersonalPosts($userId)//counts the number of personal posts made by a user
	{
		//$st = $this->_driver->prepare('CALL sp_CountUserPersonalPosts(:userId)');
		$st = $this->_driver->prepare("SELECT count(*) as count from UserPost where UserId = :userId");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->execute();
		$st->bindColumn('count',$count);
		$row = $st->fetch();
		return $count;
	}
	
	
	public function countUserPhotos($userId)//counts the number of photos uploaded by a user 
	{
		//$st = $this->_driver->prepare('CALL sp_CountUserPhotos(:userId)');
		$st = $this->_driver->prepare("SELECT count(*) as count from UserPhoto where UserId = :userId");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->execute();
		$st->bindColumn('count',$count);
		$row = $st->fetch();
		return $count;
	}
	
	
	public function countUserVideos($userId)//counts the number of videos uploaded by a user
	{
		//$st = $this->_driver->prepare('CALL sp_CountUserVideos(:userId)');
		$st = $this->_driver->prepare("SELECT count(*) as count from UserVideos where UserId = :userId");	
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);	
		$st->execute();
		$st->bindColumn('count',$count);
		$row = $st->fetch();
		return $count;
	}
	
	public function getCityNameByCityId($cityId)//fetches the name of a city when give the cityid
	{

		$st = $this->_driver->prepare("SELECT CityName  from cities  where cityID =:cityId");	
		$st->bindValue(':cityId', $cityId, PDO::PARAM_STR);		
		$st->execute();
		$row = $st->fetch();
		return $row["CityName"]; 
			
	}
	
	
	public function getStateNameByStateId($stateId)//fetches the name of a state when given the stateid
	{
		$st = $this->_driver->prepare("SELECT stateName  from states  where stateID =:stateId");	
		$st->bindValue(':stateId', $stateId, PDO::PARAM_STR);		
		$st->execute();
		$row = $st->fetch();
		return $row["stateName"];		
	}
	
	public function getCountryNameByCountryId($countryId)//fetches the name of a country when given the countryId
	{
		$st = $this->_driver->prepare("SELECT countryName  from countries  where countryID =:countryId");	
		$st->bindValue(':countryId', $countryId, PDO::PARAM_STR);		
		$st->execute();
		$row = $st->fetch();
		return $row["countryName"];
	}
	
	public function hideAppearance($id, $userId, $palUserId)// hide appearance from a particular user
	{
		$st = $this->_driver->prepare("Insert Into UserAppearanceSettings (Id, SetterUserId, PalUserId) Values(:id, :setterUserId, :palUserId)");
		$st->bindValue(':id', $id, PDO::PARAM_STR);	
		$st->bindValue(':setterUserId', $userId, PDO::PARAM_STR);
		$st->bindValue(':palUserId', $palUserId, PDO::PARAM_STR);
		if(	$st->execute()){ return true;} else{ return false;}		
		
	}
	
	public function getAppearance($userId, $palUserId)//checks whether a user has hidden apperance from the logged in user
	{
		$st = $this->_driver->prepare("Select count(*) as count From UserAppearanceSettings Where  SetterUserId =:palUserId and (PalUserId =:userId)");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':palUserId', $palUserId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn("count", $count);	
		$st->fetch();
		return $count;	
		
	}
	
	
	public function checkUserExistence($userId)//method to check whether userId exist
	{
		$st = $this->_driver->prepare("select count(*) as count from UserAuthentication where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn("count", $count);
		$st->fetch();
		
		if($count == 1){ return true; } else {return false; }
		
	}
	
	public function fetchUrls()
	{
		$st = $this->_driver->prepare("select * from Settings");
		$st->execute();
		return $row = $st->fetch();
		
	}
	
	public function getUserAvaterField($userId)
	{
		$st = $this->_driver->prepare("select Avatar from UserBiodata  where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->execute();
		 $row = $st->fetch(PDO::FETCH_ASSOC);
		 return $row["Avatar"];
		
	}
	
//new queries 
	public function fetchCountries()
	{
		$st = $this->_driver->prepare("select countryID, countryName from countries order by countryName asc");
		$st->execute();
		return $st->fetchAll(PDO::FETCH_OBJ);
			
	}
	
	public function fetchStates($countryId)
	{
	
		$st = $this->_driver->prepare("select stateID, stateName from states Where countryId =:countryId order by stateName asc");
		$st->bindValue(":countryId", $countryId, PDO::PARAM_STR);
		$st->execute();
		return $st->fetchAll(PDO::FETCH_OBJ);
			
	}
	
	public function fetchCities($stateId)
	{
		$st = $this->_driver->prepare("select cityID, cityName from cities Where stateID =:stateId order by cityName asc");
		$st->bindValue(":stateId", $stateId, PDO::PARAM_STR);
		$st->execute();
		return $st->fetchAll(PDO::FETCH_OBJ);
				
	}
	
	public function updatePassword($hashPassword, $userId, $position )//updates the password of a user
	{
		$st = $this->_driver->prepare("Update UserAuthentication SET Password =:password, RegistrationStatus =:position Where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->bindValue(":password", $hashPassword,  PDO::PARAM_STR);
		$st->bindValue(":position", $position, PDO::PARAM_STR);
		if($st->execute()){ return true; }else{ return false; }
		
	}
	
	public function updateUserInfo($firstName, $lastName, $gender, $dateOfBirth, $userId)//adds these info from the onboard page
	{
		//$st = $this->_driver->prepare("Update UserBiodata SET FirstName =:firstname, LastName =:lastname, Gender =:gender, 
		//BirthDay =:birthday Where UserId =:userId");
		
		$st = $this->_driver->prepare("INSERT INTO UserBiodata(FirstName, LastName, Gender, BirthDay, UserId) VALUES (:firstname, :lastname,
		:gender, :birthday, :userId)");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);	
		$st->bindValue(":firstname", $firstName, PDO::PARAM_STR);
		$st->bindValue(":lastname", $lastName, PDO::PARAM_STR);
		$st->bindValue(":gender", $gender, PDO::PARAM_STR);
		$st->bindValue(":birthday", $dateOfBirth, PDO::PARAM_STR);	
		if($st->execute()){ return true; }else{ return false; }
	}
	
	public function fectchLocationDetails($cityId)//fetches stateid and countryid 
	{
		$st = $this->_driver->prepare("select stateID, countryID from cities Where cityID =:cityId");
		$st->bindValue(":cityId", $cityId, PDO::PARAM_STR);
		$st->execute();
		return $st->fetch(PDO::FETCH_ASSOC);	
	}
	
	public function updateUserLocation($userId, $cityId, $stateId, $countryId)
	{
		$st = $this->_driver->prepare("Update UserBiodata SET CityId =:cityId, StateId =:stateId, CountryId =:countryId Where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);	
		$st->bindValue(":cityId", $cityId , PDO::PARAM_STR);
		$st->bindValue(":stateId", $stateId, PDO::PARAM_STR);
		$st->bindValue(":countryId", $countryId, PDO::PARAM_STR);	
		
		if($st->execute()){ return true; }else{ return false; }
		
	}
	
	public function resetRegistrationStatus($userId, $position)
	{
		
		$st = $this->_driver->prepare("Update UserAuthentication SET  RegistrationStatus =:position Where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->bindValue(":position", $position, PDO::PARAM_STR);
		if($st->execute()){ return true; }else{ return false; }
	}

	public function getRegistrationStatus($userId)
	{
		
		$st = $this->_driver->prepare("select  RegistrationStatus FROM UserAuthentication   Where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_INT);
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		//print_r($row);

		return $row["RegistrationStatus"];
	}
	
	
	public function getLocationValues($userId)
	{
		$st = $this->_driver->prepare("select CityId, StateId, CountryId from UserBiodata where UserId = :userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->execute();
		return $st->fetch(PDO::FETCH_ASSOC);
	}
	
	public function fetchUsersByCountryId($countryId, $limit)
	{
		$st = $this->_driver->prepare("select Id,UserId from UserBiodata where CountryId = :countryId Order By Id Asc  LIMIT :limit");
		$st->bindValue(":countryId", $countryId, PDO::PARAM_STR);
		$st->bindValue(":limit", $limit, PDO::PARAM_INT);
		$st->execute();
		$row = $st->fetchAll(PDO::FETCH_ASSOC);
		return $row;
		
	}

	public function checkVerification($verificationCode)//gets username from verification table using verification code
	{
		$st = $this->_driver->prepare("select  Username FROM Verification Where Code =:code");
		$st->bindValue(":code", $verificationCode, PDO::PARAM_STR);
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		return $row["Username"];
		
	}
	
	 public function getUserIdByUserName($username)//gets userid from
	 {
	 	$st = $this->_driver->prepare("select  UserId FROM UserAuthentication Where Username =:username");
		$st->bindValue(":username", $username, PDO::PARAM_STR);
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		return $row["UserId"];
		
	}
	
	
	public function checkToken($userId, $token)//checks whether a token has been generated for user to login
	{
		$st = $this->_driver->prepare("SELECT  count(*) as count FROM UserToken Where UserId =:userId and Token =:token");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->bindValue(":token", $token, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn("count",$count);
		$st->fetch();
		if($count == 1){ return true;} else{ return false;}
	}
		
	public function deleteToken($userId, $token)//deletes token after user must have loggen in
	{
		$st = $this->_driver->prepare("DELETE FROM UserToken Where UserId =:userId and Token =:token");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->bindValue(":token", $token, PDO::PARAM_STR);
		$st->execute();
	
	}
	
	public function deleteCode($username, $code)//deletes a codes gotten from sms after verification for sign up
	{
		$st = $this->_driver->prepare("DELETE FROM Verification   Where Username =:username and Code =:code");
		$st->bindValue(":username", $username, PDO::PARAM_STR);
		$st->bindValue(":code", $code, PDO::PARAM_STR);
		$st->execute();
	}
		
	public function updateVerifiedField($userId)
	{
		$st = $this->_driver->prepare("Update UserAuthentication Set Verified = 1 where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$st->execute();	
	}
	
	public function getUserIdFromToken($token)//tries to get userid from token
	{
		$st = $this->_driver->prepare("SELECT UserId FROM UserToken Where  Token =:token");
		$st->bindValue(":token", $token, PDO::PARAM_STR);
		$st->execute();
		$row = $st->fetch(PDO::FETCH_ASSOC);
		return $row["UserId"];
	}
		
}