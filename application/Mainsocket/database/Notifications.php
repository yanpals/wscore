<?php


use \Mf_Core\Registry;
use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;
use Mf\Mainsocket\database\DatabaseComments;


class DatabaseNotifications extends Database{
	
	protected $_UserDb;
	protected $_PostDb;
	protected $_CommentDb;
	protected $_RateDb;
	
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
		
		//$this->_UserDb = Registry::getInstance()->get('UserDb');
		//$this->_PostDb = Registry::getInstance()->get('PostDb');
		//$this->_CommentDb = Registry::getInstance()->get('CommentDb');
		//$this->_RateDb = Registry::getInstance()->get('RateDb');
	}
	private $_cache;
	
	
	
	/*
	* @param - userId
	* @returns - User posts Array
	*/
    public function getUserNotifications($userId, $limit, $offset){
		//$st = $this->_driver->prepare("CALL sp_GetUserNotifications(:userId, :limit, :offset)");
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId   order by DateTimeLogged desc LIMIT :offset, :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':offset', $offset, PDO::PARAM_INT);
		
		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;
	}
	
	
	
	
	
	
	
	 public function getUserNotificationsNew($userId, $limit, $offset){
		//$st = $this->_driver->prepare("CALL sp_GetUserNotificationsNew(:userId, :limit, :offset)");
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId   order by DateTimeLogged desc LIMIT :offset, :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':offset', $offset, PDO::PARAM_INT);
		
		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;
	}
	
	public function loadMoreNotifications($userId, $limit, $boffset)
	{
		//$st = $this->_driver->prepare("CALL sp_LoadMoreNotifications(:userId, :limit, :Boffset)");
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId  and DateTimeLogged < :Boffset  
		order by DateTimeLogged desc LIMIT  :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':Boffset', $boffset, PDO::PARAM_INT);
		
		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;	
	}
	
	
	public function loadRecentNotifications($userId, $limit, $toffset)
	{
		//$st = $this->_driver->prepare("CALL sp_LoadRecentNotifications(:userId, :limit, :Boffset)");
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId  and DateTimeLogged > :Toffset  
		order by DateTimeLogged desc LIMIT :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':Toffset', $toffset, PDO::PARAM_INT);
		
		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;	
	}
	
	public function checkStoppedSubscription($userId, $postId)//checks whether the user has stopped notification//
	{
		$st = $this->_driver->prepare("Select count(*) as count from PostThreadUsers Where UserId =:userId and (PostId =:postId) and (Status = 1)");
		$st->bindValue(':userId', $userId,PDO::PARAM_STR );
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$st->fetch();
		if($count > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	public function checkSubscriptionOnRate($userId, $postId)//checks whether the person is recieving notification on rate
	{
		$st = $this->_driver->prepare("Select count(*) as count from PostThreadUsers Where UserId =:userId and PostId =:postId 
		and ActivityType = 2");
		$st->bindValue(':userId', $userId,PDO::PARAM_STR );
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$st->fetch();
		return $count;
	}
	
	public function checkSubscriptionOnComment($userId, $postId)//checks whether the person is recieving notification on comment
	{
		$st = $this->_driver->prepare("Select count(*) as count from PostThreadUsers Where UserId =:userId and PostId =:postId and ActivityType = 1" );
		$st->bindValue(':userId', $userId,PDO::PARAM_STR );
		$st->bindValue(':postId', $postId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$st->fetch();
		return $count;
	}
	
	
	public function trash($notificationId){
	 	$st = $this->_driver->prepare("CALL sp_TrashNotification(:notificationId)");
		$st->bindValue(":notificationId", $notificationId, PDO::PARAM_INT);
		if($st->execute()){
			return true;
		}
		else{
			return false;
		}
		
		$st = NULL;
	}
	

	public function markAsRead($notificationId){
		//$st = $this->_driver->prepare("CALL sp_MarkAsReadNotification(:notificationId)");
		$st = $this->_driver->prepare("Update Notification set Status = 2 where Id =:notificationId");
		$st->bindValue(":notificationId", $notificationId, PDO::PARAM_INT);
		$result = ($st->execute())? true : false;
		return $result;	
		$st = NULL;
		
	}
	
	
	
	
	
	
	public function UpdateAllToSeen($userId){//will update all the notifications of a user to seen when he/she clicks the icon
		$st = $this->_driver->prepare("Update Notification set Status = 1 where UserId =:userId");
		$st->bindValue(":userId", $userId, PDO::PARAM_STR);
		$result = ($st->execute())? true : false;
		return $result;	
		$st = NULL;
		
	}
	
	
	
	public function countUserNotifications($userId){//counts all the notifications of a user
		
		$st = $this->_driver->prepare("CALL sp_CountUserNotification(:userId)");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$result = $st->fetch(PDO::FETCH_ASSOC);
		$st = NULL;
		return $count;
	}
	
	
	public function countUserUnseenNotifications($userId){//counts all the notifications which a user has not seen
		
		$st = $this->_driver->prepare("CALL sp_CountUserUnreadNotification(:userId)");
		//$st = $this->_driver->prepare("select count(*) as count from Notification where UserID =:userId and Status = 0");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$result = $st->fetch(PDO::FETCH_ASSOC);
		$st = NULL;
		return $count;
	}
	
	public function notifyPostOwner($comment, $notificationArea){
		
		$postDb = $this->_PostDb;
		$postOwner = $this->_PostDb->getPostOwner($comment->postId);
		
		//check if the commenter is not the Post owner
		if($postOwner != $comment->userId){
			if(!$this->checkStoppedSubscription($postOwner, $comment->postId)){//check whether the person has stopped notification
			
				$userDb = $this->_UserDb;
				
				$PostId = $comment->postId;
				
				$contArray = json_encode(array("PostId"=>$comment->postId, "CauseUserId"=>$comment->userId));//creates an array to hold the content of            	the notification, the postId involved ant the userid of the person that made the notification to take place
				
				$status  = 0;
				$dateTimeLogged = time();
				$userId = $postOwner;
				$type = 1;
				$this->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);
			}
		}
		return true;

	}
	
	
	public function notifyOtherCommenters($comment, $notificationArea)
	{
		$commentDb = $this->_CommentDb;
		$postDb = $this->_PostDb;
		$userDb =  $this->_UserDb;
		//fetch Poster Name using the postId
		$posterName = $userDb->fetchUserName($postDb->getPostOwner($comment->postId));
		$postId = $comment->postId;
		
		$postOwnerUserId = $postDb->getPostOwner($comment->postId);
		
		
		$contArray = json_encode(array("PostId"=>$comment->postId, "CauseUserId"=>$comment->userId));
		//creates an array to hold  the postId involved and 	the userid of the person that made the notification to take place	
		
		$result = $commentDb->getPostCommenters($comment->postId);//fetch Other Commenters for that post
		 
		foreach($result as $res){
			if($comment->userId != $res["UserId"]  && $res["UserId"] != $postOwnerUserId ){
				//this prevents the commenter from being notified and the postowner from recieving double notifications
					
				$status  = 0;
				$dateTimeLogged = time();
				$userId = $res["UserId"];
				$type = 1;
				$this->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);
				
			}//ends first if
		}//ends foreach
		
	   return true;	
	}//ends function
	
	
	
	public function notifyPostOwnerOnrate($rate, $notificationArea)
	{
		$postDb = $this->_PostDb;
		$postOwner = $postDb->getPostOwner($rate["PostId"]);	
		//check if the commenter is not the Post owner
		if($postOwner != $rate["UserId"]){
			if(!$this->checkStoppedSubscription($postOwner, $rate["PostId"])){//if the user hasn't stopped notification 
		
				$userDb = $this->_UserDb;
				$postId = $rate["PostId"];
				$userId =  $postOwner;
				$type = 2;
				$dateTimeLogged = time();
				$status = 0;
				$contArray = json_encode(array( "PostId"=>$rate["PostId"], "CauseUserId"=>$rate["UserId"]));//creates an array to hold the content of 					             	 notification, the postId involved ant the userid of the person that made the notification to take place	
				$this->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);
			}
		}
		return true;

	}
	

	
	
	public function notifyOtherRaters($rate, $notificationArea)
	{
		$rateDb = $this->_RateDb;
		$postDb = $this->_PostDb;
		$userDb = $this->_UserDb;
		
		
		//fetch Poster Name using the postId
		$postOwner = $postDb->getPostOwner($rate["PostId"]);	
		$poster = $userDb->fetchUserName($postOwner);
		
		
		$contArray = json_encode(array("PostId"=>$rate["PostId"], "CauseUserId"=>$rate["UserId"]));//creates an array to hold the content of 					        notification, the postId involved ant the userid of the person that made the notification to take place	
				
		$result = $rateDb->getPostRaters($rate["PostId"]);	//fetch Other raters for that post							
		
		foreach($result as $res){
			if($rate["UserId"] != $res['UserId']){ //this prevents the rater from being notified
				
				$userId = $res['UserId'];
				$content = $content;
				$type = 2;
				$dateTimeLogged = time();
				$status = 0;
				$this->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);
				
			}
		}
		
	   return true;	
	}
	
	
	public function notifyRebroadcast($rebroadcasterUserId, $originalPostIdRemains, $notificationArea)
	{
		$rateDb = $this->_RateDb;
		$postDb = $this->_PostDb;
		$userDb = $this->_UserDb;
		
		
		
		
		$rebroadcasterName = $userDb->fetchUserName($rebroadcasterUserId);
		
		$PostOwnerUserId = $postDb->getPostOwner($originalPostIdRemains);
		
		if($PostOwnerUserId != $rebroadcasterUserId){
			
				$contArray = json_encode(array( "PostId"=>$originalPostIdRemains, "CauseUserId"=>$rebroadcasterUserId));
				//creates an array to hold notification,postId and the userid of the person that made the notification to take place
				
				$this->deposit($PostOwnerUserId, $contArray, 3,  time() , 0, $notificationArea);
			
		}
	}
	
	
	public function notifyPalrequestAcceptance($userId, $CauseUserId, $notificationArea)/*this will notify a user that his palrequest
	 has been accepted. userId is the one who sent while the causerUserId is the reciever*/
	{
		$contArray = json_encode(array("CauseUserId"=>$CauseUserId));/*creates an array to hold the 
		userid of the person that made the notification to take place	*/
		
		$this->deposit($userId,  $contArray, 4,  time() , 0, $notificationArea);
	}
	
	
	
	public function deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea)
	{
		$st = $this->_driver->prepare("CALL sp_AddNotification(:userId, :content, :type, :dateTimeLogged, :status,  :notificationArea)");
		$st->bindValue(':userId', $userId,PDO::PARAM_STR );
		$st->bindValue(':content', $contArray, PDO::PARAM_STR);
		$st->bindValue(':type', $type, PDO::PARAM_INT);
		$st->bindValue(':dateTimeLogged', time(), PDO::PARAM_STR);
		$st->bindValue(':status', $status, PDO::PARAM_INT);
		$st->bindValue(':notificationArea', $notificationArea, PDO::PARAM_INT);
		$st->execute();
		$st = NULL;
	}
	
}