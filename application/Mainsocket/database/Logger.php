<?php
/*
 *
 *This class queries the comments in the database
 *
*/

use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;



Class DatabaseLogger extends Database
{
	
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('wokon_user_statistics');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokon_user_statistics', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
	}
	
	
	
	
	//counts the number of comments of a post
	public function logUserInteraction(Array $data)
	{

		
		//$st = $this->_driver->prepare('CALL sp_LogUserInteraction(:userId, :viewerId, :objectId, :objectTypeId, :privacy, :interactionType, :interactionLevel, :date)');
		$query = 'insert into UserInteractionScale(UserId,ViewersId, ObjectId,ObjectTypeId, Privacy, InteractionType,InteractionLevel,Date)values(:userId,:viewerId, :objectId, :objectTypeId, :privacy, :interactionType, :interactionLevel, :date)';
		$st = $this->_driver->prepare($query);
		
		/*$st->bindValue(':userId', $data["userId"], PDO::PARAM_STR);
		$st->bindValue(':viewerId', $data["viewerId"], PDO::PARAM_STR);
		$st->bindValue(':objectId', $data["objectId"], PDO::PARAM_STR);
		$st->bindValue(':objectTypeId', $data["objectTypeId"], PDO::PARAM_INT);
		$st->bindValue(':privacy', $data["privacy"], PDO::PARAM_INT);
		$st->bindValue(':interactionType', $data["interactionType"], PDO::PARAM_INT);
		$st->bindValue(':interactionLevel', $data["interactionLevel"], PDO::PARAM_INT);
		$st->bindValue(':date', date('Y-m-d'), PDO::PARAM_STR);*/
		//$st->execute();
		
		foreach ($data as $key => $value) {
			# code...
			if(in_array($key, array('objectTypeId', 'objectId', 'privacy', 'interactionType', 'interactionLevel')) !== false){
				$st->bindValue(':'.$key, $value, PDO::PARAM_INT);
			}else{
				$st->bindValue(':'.$key, $value, PDO::PARAM_STR);
			}
		}
		
		
		
		
		
		return $st->execute() ? true :false;
	}
	
	
	
 
}//ends class