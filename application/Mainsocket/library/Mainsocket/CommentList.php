<?php
namespace Mf\Mainsocket\library\Mainsocket;
use Mf\Mainsocket\library\Mainsocket\Comment;

class CommentList{

	public $commentsArray = array();


	//collects comments from Databasecomment class as an array and sends them to createComments method
	public function __construct(Array $comments){
		$this->createComments($comments);
	}
	
	
	//loopsthrough the comments , create new comments objects  and stores them in an array
	private function createComments(Array $comments){
		foreach($comments as $comment){
			$this->commentsArray[] = new Comment($comment);
		}
	}
	
	//this method get the commentid of a rate loops through the list, fetches the properties of the commment
	public function getCommentById($commentId){
		foreach($this->commentsArray as $comment){
			if($comment->get('commentId') == $commentId){
			   return $comment;
			}
		}	
	}
	
	
	public function get($key){
		$newKey = $key;
		//$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}
	
}