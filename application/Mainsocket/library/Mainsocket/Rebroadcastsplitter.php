<?php

use \Mf_Core\Registry;
use \Mf_Core\Session;



defined('_PUBLIC') || exit;

class Rebroadcastsplitter{

	public $postId;
	public $postType;
	public $content;
	public $media;
	public $privacy;
	public $time;
	public $modifiedTime;
	
	public $userId;
	
	
	
	public $userInfo;
	public $trashed;
	public $isOwner;
	
	public $viewers;
	public $viewersCount = 0;
	
	//comments
	public $commentCount;
	//public $commentFetched;
	//public $commentList;
	
	//Comment Database Object
	private $_db;
	
	//Rates
	public $rateCount;
	public $rateFetched;
	public $rateList;
	
	
	public $limit = 1000;
	public $rateWeight;
	public $hasRated;
	public $noOfRebroadcast;
	
	public $originalPostId;
	public $lastRebroadcastId;
	

	public function __construct($options, $loggedInUserId)
	{
		
		$this->postId = isset($options['PostId']) ? $options['PostId'] : NULL;
		$postType = isset($options['PostType']) ? $options['PostType'] : NULL; 
		switch($postType){
			case 1:
				$this->postType = 'text';
				break;
			case 2:
				$this->postType = 'image';
				break;
			case 3:
				$this->postType = 'video';
				break;
			case 4:
				$this->postType = 'link';
				break;
		}
		$content = isset($options['Content']) ? $options['Content'] : NULL; 
		
		$this->time = isset($options['Time']) ? $options['Time'] : NULL;
		$this->ModifiedTime = isset($options['ModifiedTime']) ? $options['ModifiedTime'] : NULL;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : NULL;//\Mf_Core\Registry::getInstance()->get('user');
		$this->trashed = isset($options['Trashed']) ? $options['Trashed'] : NULL;
		$this->isOwner = ($loggedInUserId == $this->userId) ? true : false;
		$this->privacy = isset($options['Privacy']) ? $options['Privacy'] : NULL;  
		$this->originalPostId = isset($options['OriginalPostId']) ? $options['OriginalPostId'] : NULL;   
		$this->lastRebroadcastId = isset($options['LastRebroadcastId']) ? $options['LastRebroadcastId'] : NULL;   
		$media = isset($options['Media']) ? json_decode($options['Media']) : NULL;
		
		$this->arrangeContent($content, $loggedInUserId, $this->userId);
		$this->countRebroadcast($this->postId);
		$this->checkRate($loggedInUserId, $this->postId);//check whether the loggedin user has rated
		
		
		
		if($media !== NULL ){//may not be used depending on whether a rebroadcast should have media
			$this->media = $this->arrangeMedia($media);
		}
		else{
			$this->media = null;
		}
		
		
			//get User Object
			$this->userInfo = $this->_getUser(); 
				
			//$this->commentList = $this->_getComments();
			$this->rateList = $this->_getRates();
			
			
			//privacy Stting: 1 - Public, 2 - Private
			$this->viewers = ($this->privacy == 2) ? $this->_getPostViewers() : NULL;
		
		
		//$this->getRateWeight();
	
		
	}
	
	
	
	
	public function  getRebroadcast()
	{
		$rebArray = array("postId"=>$this->postId, "postType"=>$this->postType,"content"=>$this->content,"media"=>$this->media,"privacy"=>$this->privacy,"time"=>$this->time,"modifiedTime"=>$this->modifiedTime, "userId"=>$this->userId, "userInfo"=>$this->userInfo, "trashed"=>$this->trashed, "isOwner"=>$this->isOwner, "commentCount"=>$this->commentCount, "rateCount"=>$this->rateCount, "rateWeight"=>$this->rateWeight, "noOfRebroadcast"=>$this->noOfRebroadcast,"originalPostId"=>$this->originalPostId, "lastRebroadcastId"=>$this->lastRebroadcastId);
		
		return $rebArray;
		
}

	public function get($key)
	{
		$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}
	
	private function _getComments()
	{
		//create comment database instance
		$this->_db = new DatabaseComments;
		$comments =  new CommentList($this->_db->getCommentByPostId($this->postId, $this->limit, 0));
		$commentA = array();
		foreach($comments->get('commentsArray') as $comment){
		   	$commentA[] = $comment;
		}
		$this->commentFetched = count($commentA);
		$this->commentCount = $this->_countPostComments();
		return $commentA;
	}
	
	private function _countPostComments()
	{
		return $this->_db->countPostComments($this->postId);
	}
	
	private function _getRates()
	{
		$this->_db = new DatabaseRates;
		$rates = new RateList($this->_db->getRatesByPostId($this->postId,  $this->limit, 0));
		$ratesArray = array();
		$ratesweight = 0;
		foreach($rates->getRateArray() as $rate){
		   	$ratesArray[] = $rate;
			$ratesweight  = $ratesweight + $rate->rateLevel;
		}
		$this->rateFetched = count($ratesArray);
		$this->rateCount = $this->_countPostRates();
		$this->rateWeight = $ratesweight;	
		return $ratesArray;
			
	}
	
	
	
	
	
	private function _countPostRates()
	{
		return $this->_db->countPostRates($this->postId);
	}
	
	private function _getUser()
	{
		//connect to user database and get UserInfo as Object	
		$this->_db = new DatabaseUser;
		$userInfo = $this->_db->fetchUser($this->userId);
		
		//make sure result returned fro query is an array
		if(is_array($userInfo)){
			return new YanPalUser($userInfo); 
		}
		return;
	}
	
	private function _getPostViewers()
	{
		$this->_db = new DatabasePosts;
		$users = $this->_db->getPostViewers($this->postId);
		
		//set ViewersCount
		$this->viewersCount = count($users);
		
		$this->_db = new DatabaseUser;
		$viewers = '';
		foreach($users as $userId){
			$viewers .= $this->_db->fetchUserName($userId['UserId']) . '\r\n';	
		}
		return trim($viewers, '\r\n');
	}
	
	
	
	
	private function checkRate($loggedInUserId, $postId)
	{
		$db = new  DatabasePosts;
		
		$this->hasRated = $db->checkRate($loggedInUserId, $postId);
	}
	
	
	private function  countRebroadcast($postId)
	{
		
		$db = new  DatabasePosts;
		$this->noOfRebroadcast = $db->countRebroadcast($postId);
	}
	
	
	//this method adds url to the media
		private function arrangeMedia($media)
	{
		$urls = json_decode(\Mf_Core\Registry::getInstance()->get('session')->read('loadUrls'), true);//gets url from session
		
		$originalpath = $urls["OriginalPhotoUrl"];//collects the original path
		$resizedpath = $urls["ResizedUrl"];//collects  the resized path
		$croppedpath = $urls["AvatarUrl"];
		$pathVideo = $urls["VideoUrl"];//collects video path
		$pathThumbnail = $urls["OriginalPhotoUrl"];//video thumbnail path
		
		
		$arrayMedia = $media;//its an array
	
		foreach($arrayMedia as $mediaItem){
			if($mediaItem->type == 'image'){//if it is an image//
			//	var_dump($arrayMedia); die;
				
					
						//list($width,$height) = getimagesize($originalpath.$mediaItem->src);//gets the size of the image
						$mediaItem->original = array("src" =>$originalpath.$mediaItem->src);//form an array for
					
				
				
					//list($width2,$height2) = getimagesize($resizedpath.$mediaItem->src);//gets the size of the image
					$mediaItem->resized = array("src" =>$resizedpath.$mediaItem->src);
				
	
				
				
				
				//list($width3,$height3) = getimagesize($croppedpath.$mediaItem->src);//gets the size of the image
				//$mediaItem->cropped = array("src" =>$croppedpath.$mediaItem->src, "width"=>$width3, "height"=>$height3);
				
				
				unset($mediaItem->src);

			}
			elseif($mediaItem->type == 'video'){// if its a video
				//echo "video_found";
				
				$mediaItem->src = $pathVideo.$mediaItem->src;
				$mediaItem->poster = $pathThumbnail.$mediaItem->poster;
			}
		}
		return $arrayMedia;
	}

	
	public function arrangeContent($content, $loggedInUserId, $userId)
	{
		if($content != "First Post with profile picture"){
			$this->content = $content;
		}
		else{
			if($loggedInUserId == $userId){
				$this->content = "You Uploaded your first profile picture";	
			}
			else{
				$this->_db = new DatabaseUser;
				$userInfo = $this->_db->fetchUser($userId);
				
				if(is_array($userInfo)){
					
					if($userInfo["Gender"] == "female")
					{
						$gender= "her";
					}
					else{
						$gender = "his";
					}
					
				 	$this->content = $userInfo["FirstName"]. " ".$userInfo["LastName"]. " " ."uploaded".   "  " .$gender. "  ". "profile picture";
				}
			}
			
		}
	}
	
	
}//end  of class