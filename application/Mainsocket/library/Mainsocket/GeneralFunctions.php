<?php
namespace Mf\Mainsocket\library\Mainsocket;

class GeneralFunctions
{

	public function IdGenerator()
	{
		$word = "abdeghijklmnqstuwxyz012456789";
		$randomKey = str_shuffle($word);
		$key = substr($randomKey,0,9);
		$id = uniqid('',true).$key;
		return $id;

	}

	public function profilePhotoIdGen()
	{
		$word = "abdeghijklmnqstuwxyz012456789";
		$randomKey = str_shuffle($word);
		$key = substr($randomKey,0,6);
		$id = uniqid('',true).$key."PRO";
		return $id;
	}


	public function profileHeaderVideoIdGen()
	{
		$word = "abdeghijklmnqstuwxyz012456789";
		$randomKey = str_shuffle($word);
		$key = substr($randomKey,0,6);
		$id = uniqid('',true).$key."VID";
		return $id;
	}

	public function profileHeaderPhotoIdGen()
	{
		$word = "abdeghijklmnqstuwxyz012456789";
		$randomKey = str_shuffle($word);
		$key = substr($randomKey,0,5);
		$id = uniqid('',true).$key."COV";
		return $id;
	}


	public function CommentIdGenWithVideo()
	{
		$word = "abdeghijklmnqstuwxyz012456789";
		$randomKey = str_shuffle($word);
		$key = substr($randomKey,0,5);
		$id = uniqid('',true).$key."CMV";
		return $id;
	}


	public function CommentIdGenWithPhoto()
	{
		$word = "abdeghijklmnqstuwxyz012456789";
		$randomKey = str_shuffle($word);
		$key = substr($randomKey,0,5);
		$id = uniqid('',true).$key."CMP";
		return $id;
	}


	public function OrdinaryIdGen()
	{
		return uniqid("", true);
	}


	public function sanitizeInput($input)
	{
		$input = filter_var($input, FILTER_SANITIZE_STRING);
		//$input = preg_replace("/&#?[a-z0-9]+;/i","",$input);
		$input = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $input);
		return $input;
	}

}
