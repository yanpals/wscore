<?php
/**
 *
 */
use Mf_Core\User\AuthHandler;

/**
 * Authention handler to authenticate
 * Wokon User.
 */
class WokonUserAuthHandler extends AuthHandler
{
	/**
	 *
	 */
	public function __construct(array $options)
	{
		parent::__construct($options);
	}
}