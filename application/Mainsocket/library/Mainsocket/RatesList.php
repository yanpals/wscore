<?php
namespace Mf\Mainsocket\library\Mainsocket;
use Mf\Mainsocket\library\Mainsocket\Rate;


class RateList{

	public $ratesArray = array();
	
	
	//collects rates from Databaserate class as an array and sends them to createRates method
	public function __construct(Array $rates){
		$this->createRates($rates);
	}
	
	
	//loopsthrough the rates , create new Rate objects  and stores them in an array
	private function createRates(Array $rates){
		foreach($rates as $rate){
			$this->ratesArray[] = new Rate($rate);
		}
	}
	
	//this method get the rateid of a rate loops through the list, fetches the properties of the rate
	public function getRateById($rateId){
		foreach($this->ratesArray as $rate){
			if($rate->get('rateId') == $rateId){
			   return $rate;
			}
		}	
	}
	
	//this method gets the value from a property of a class
	public function get($key)
	{
		//$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		$newKey = $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}
	
	public function getRateArray()
	{
		return $this->ratesArray;
	}
}