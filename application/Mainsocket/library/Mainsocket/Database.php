<?php
/**
 *
 */
use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;

/**
 * Base Database
 */
class WokonDatabase extends Database
{
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('database');

		$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
		parent::__construct('wokondb', $driver);
	}
}
