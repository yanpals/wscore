<?php
// Direct access check
defined('_PUBLIC') || exit;

class NotificationLogger{

	public $notificationsArray = array();

	public function __construct(Array $notifications){
		$this->db = new DatabaseNotifications;
		 $this->_createNotification($notifications);
	}
	
	/*
	* Write New notification into the db
	* @param - notification object
	*/
	public function write(Notifiction $notification){
		$this->db->writeNotification($notification);
	}
	
	/*
	* Return an array of notifications for a particular user
	* @param $userId
	* @return Array of Notification Objects 
	*/
	public function Read($userId){
		$notArray = $this->db->readNotifications($userId);	
		$this->_createNotification($notArray);
		return $this->get('notificationArray');
	}
	
	/**
	* Create a notification Object and append to notification array
	* @param Notifications array
	*/
	private function _createNotification(Array $notifications){
		foreach($notifications as $notification){
			$this->notificationsArray[] = new Notification($notification);
		}
	}
	
	public function getNotificationArray()
	{
		return $this->notificationsArray;
	}
	
	
	public function get($key){
		$newKey = $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}
	
	
}