<?php
// Direct access check
defined('_PUBLIC') || exit;

class Notification{
	
	public  $id; 
	public $userId; 
	public $type; 
	public $dateTimeLogged; 
	public $dateTimePushed;
	public $seen;
	public $postId;
	public $postOwnerUserId;
	public  $causeUserinfo; 
	public $postOwnerDetails;
	public $postDetails;
	public $isOwner;
	public $unRead;
	public $notificationArea;
	public $statement; 
	
	
	public function __construct(Array $options){
		$this->id = isset($options['Id']) ? $options['Id'] : NULL;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : NULL;
		$contArray = isset($options['Content']) ? json_decode($options['Content']) : NULL;
		$this->dateTimeLogged = isset($options['DateTimeLogged']) ? $options['DateTimeLogged'] : NULL;
		$this->dateTimePushed = isset($options['DateTimePush']) ? $options['DateTimePush'] : NULL;
		$this->postId = isset($contArray->PostId) ? $contArray->PostId : NULL;
		$notificationArea = isset($options['NotificationArea']) ? $options['NotificationArea'] : NULL;
		$causeUserId = isset($contArray->CauseUserId) ? $contArray->CauseUserId : NULL;
		$status = isset($options['Status']) ? $options['Status'] : NULL;
		$type = isset($options['Type']) ? $options['Type'] : NULL;
		
		$this->_setStatus($status);
		$this->swapType($type);//get type, types include comment, rate, rebroadcast, palrequest etc.
		
		if($causeUserId != NULL){
			$this->causeUserinfo = $this->getUser($causeUserId);//the information of the person that caused the notification
		}
		else{
			$this->causeUserinfo = NULL;
		}
		
		$this->_setNotificationArea($notificationArea);
		
		
		if($this->postId != NULL){
			$this->postOwnerUserId = $this->getPostownerUserId($this->postId);
			
		}
		
		$this->postOwnerDetails = $this->getPostownwerdetails($this->postOwnerUserId);
		
		$this->isOwner = $this->_isOwnerFunction($this->userId, $this->postOwnerUserId);
		
		$this->statement = $this->constructDisplay($this->userId, $causeUserId, $this->type, $this->postOwnerUserId);//will construct the statement that will be displayed
		
		
	}//ends __construct
	
	private function _isOwnerFunction($user1, $user2)
	{
		if($user1 !== $user2){
			return "false";
		}
		else{
			return "true";
		}
	}
	
	
	private function getUser($causeUserId){
		//connect to user database and get UserInfo as Object	
		$db = new DatabaseUser;
		$userInfo = $db->fetchUser($causeUserId);
		
		//make sure result returned fro query is an array
		if(is_array($userInfo)){
			return new YanPalUser($userInfo); 
		}
		return;
	}
	
	/*
	* Returns instance variables if they exist
	*/
	public function get($key){
		$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}
	
	public function getPostownerUserId($postId)
	{
		$db = new DatabasePosts();
		$this->postDetails = $db->getPostById($postId);
		return $db->getPostOwner($postId);
		
	}
	
	public function swapType($type)//swaps the type of notification
	{
		if($type == 1){
			$this->type = "comment";
		}
		else if($type == 2){
			$this->type = "rate";
		}
		
		else if($type == 3){
			$this->type = "rebroadcast";
		}
		else if($type == 4){
			$this->type = "palRequest";
		}
		
		else {
			$this->type = NULL;
		}
	}
	
	
	private function _setStatus($status)//this function sets the status for a nofitication, 0 stands for not seen, 1 for seen but not read ,2 for read
	{
		if($status != NULL){
			
			if($status == 0){
				$this->seen = 0;//not seen by the user
			}
			if($status == 1)
			{
				$this->seen = 1;//seen by the user
			}
			
			
			if($status == 2){
				$this->unRead = 1;// has been read
			}
			else{
				$this->unRead = 0;//not read 
			}
			
		}//ends first if
	}//ends function _setStatus
	
	
	
	private function getPostownwerdetails($postOwnerUserId)
	{
		//connect to user database and get UserInfo as Object	
		$db = new DatabaseUser;
		$userInfo = $db->fetchUser($postOwnerUserId);
		
		//make sure result returned fro query is an array
		if(is_array($userInfo)){
			return new YanPalUser($userInfo); 
		}
		return;
	}
	
	private function _setNotificationArea($notificationArea)
	{
		if($notificationArea == 1){
			$this->notificationArea = "board";
		}
		else if($notificationArea == 2){
			$this->notificationArea = "clique";
		}
		
		else if($notificationArea == 3){
			$this->notificationArea = "pages";
		}
		else if($notificationArea == 4){
			$this->notificationArea = "careers";
		}
		
		else {
			$this->notificationArea = NULL;
		}
		
	}
	
	public function constructDisplay($userId, $causeUserId, $type, $postOwnerUserId)
	{
		$statement = "";
		if($type == "palRequest"){
			$statement = $this->causeUserinfo->firstName ." ".$this->causeUserinfo->lastName . "  Accepted your Pal Request";	
		}
		else if($type == "rate")
		{
			$statement = $this->causeUserinfo->firstName ." ".$this->causeUserinfo->lastName . " Rated your post";	
		}
		
		else if($type == "comment")
		{
			$statement = $this->causeUserinfo->firstName ." ".$this->causeUserinfo->lastName ;
			if($userId == $postOwnerUserId){//if someone commented on your post
				$statement .=" Commented On Your post"; 
			}
			if(($userId != $postOwnerUserId) && ($postOwnerUserId != $causeUserId))//if someone commented on another person comment which u also commented
			{
				$statement .=" Also Commented On ". $this->postOwnerDetails->firstName ." ".$this->postOwnerDetails->lastName . "'s  Post";
			}
			
			if(($userId != $postOwnerUserId) && ($postOwnerUserId == $causeUserId))//if someone commented on his/her post which u also commented
			{
				$pronoun = ($this->postOwnerDetails->firstName == "female")? "her":"his";
				$statement .=" Also Commented On " .$pronoun. "  Post";
			}
		}
		return $statement;
	}
	
}