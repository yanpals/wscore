<?php
namespace Mf\Mainsocket\library\Mainsocket;
use  Mf\Mainsocket\library\Mainsocket\YanPalUser;
use \Mf_Core\Registry;
/*
 *
 *
 *
*/

Class Comment
{

	public $commentId;
	public $momentId;
	public $content;
	public $time;
	public $userId;
	public $trashed;
	public $userInfo;
	public $url;
	private $UserDb;


	//construct method collects a linear array of a comment, splits the array to get the properties of the comment
	public function __construct($options, $fetchComplete = true)
	{
		$this->UserDb = Registry::getInstance()->get('UserDb');
		$this->commentId = isset($options['CommentId']) ? $options['CommentId'] : NULL;
		$this->momentId = isset($options['MomentId']) ? $options['MomentId'] : NULL;
		$this->content = isset($options['Content']) ? $options['Content'] : NULL;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : NULL;
		$this->time = isset($options['Time']) ? $options['Time'] : NULL;
		$this->trashed = isset($options['Trashed']) ? $options['Trashed'] : NULL;
		$this->modifiedTime = isset($options['ModifiedTime']) ? $options['ModifiedTime'] : NULL;

		if($fetchComplete){
			$this->userInfo = $this->_getUser($this->userId);
		}



	}


	//this method gets the value from a property of a class
	public function get($key)
	{
		$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}

	public function arrangeUrl($filename, $type)
	{
		$urls = json_decode($_SESSION["urls"], true);//gets url from session
		if($type == 'image'){
		$resizedpath = $urls["ResizedUrl"];//collects  the resized path
		$this->src = $resizedpath.$filename;
		}
		if($type == 'video'){
			$this->src = $urls["VideoUrl"].$filename;
		}
	}


	private function _getUser($userId)
	{
		$userInfo = $this->UserDb->fetchUser($userId);
		if(is_array($userInfo)){
			return new YanPalUser($userInfo);
		}else{
		return  null;
	 }
	}





}
?>
