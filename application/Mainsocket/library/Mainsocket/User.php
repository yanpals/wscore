<?php
namespace Mf\Mainsocket\library\Mainsocket;
//use Mf_Core\User;
use Mf_Core\Database;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\User;

/**
 * Wokon User
 The avatar field in the database is  a json stored object in the following format
 {"profilePhoto":"avatar_1.jpg","coverPhoto":"cover_1.jpg"}
 */
class YanPalUser //extends User
{
	public $userId;
	public $firstName;
	public $lastName;
	public $gender;
	public $cityId;
	public $birthday;
	public $email;
	public $phone;
	public $settings;
	public $onlineStatus;
	public $accountStatus;
	public $device;
	public $username;
	public $accountType;
	public $dateJoined;
	public $stateId;
	public $countryId;
	public $avatar;
	public $coverPhoto;
	public $avatarSmall;
	public $website;
	public $profileVideo;
	public $cityName;
	public $stateName;
	public $countryName;
	public $urlName;
	protected $_UserDb;
	

	/**
	 *
	 */
	protected static $_instance;

	/**
	 * Creates instance of User
	 */
	public function __construct(array $options)
	{
		//parent::__construct($options);
		$this->_UserDb = $this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->userId = isset($options['UserId']) ? $options['UserId'] : 0;
		$this->firstName = isset($options['FirstName']) ? ucfirst($options['FirstName']) : NULL;
		$this->lastName = isset($options['LastName']) ? ucfirst($options['LastName']) : NULL;
		$this->gender = isset($options['Gender']) ? $options['Gender'] : NULL;
		$this->cityId = isset($options['CityId']) ? $options['CityId'] : NULL;
		$this->birthday = isset($options['Birthday']) ? $options['Birthday'] : NULL;
		$this->email = isset($options['Email']) ? $options['Email'] : NULL;
		$this->phone = isset($options['Phone']) ? $options['Phone'] : NULL;
		$this->settings = isset($options['Settings']) ? $options['Settings'] : NULL;
		$this->onlineStatus = isset($options['OnlineStatus']) ? $options['OnlineStatus'] : NULL;
		$this->accountStatus = isset($options['AccountStatus']) ? $options['AccountStatus'] : NULL;
		$this->device = isset($options['Device']) ? $options['Device'] : NULL;
		$this->username = isset($options['Username']) ? $options['Username'] : NULL;
		$this->accountType = isset($options['AccountType']) ? $options['AccountType'] : NULL;
		$this->dateJoined = isset($options['DateJoin']) ? $options['DateJoin'] : NULL;
		$this->stateId = isset($options['StateId']) ? $options['StateId'] : NULL;
		$this->countryId = isset($options['CountryId']) ? $options['CountryId'] : NULL;
		$this->cityId = isset($options['CityId']) ? $options['CityId'] : NULL;
		$this->website = isset($options['Website']) ? $options['Website'] : NULL;
		$this->urlName = isset($options['UrlName']) ? $options['UrlName'] : NULL;//this will be used for the person profile page
		$this->_arrangeUrls($options);
		
		if($this->cityId !== NULL){
			$this->_fetchUserCityName($this->cityId);
		}
		if($this->stateId !== NULL){
			$this->_fetchStateName($this->stateId);
		}
		if($this->countryId !== NULL){
			$this->_fetchUserCountryName($this->countryId);
		}
		
	}

	/**
	 * Gets instance of User using application
	 *
	 * @override
	 */
	public static function getInstance($id = 0)
	{
		if(!self::$_instance) {
			if(!$id)
			{
				@self::$_instance = new self( array("id"=>$id) );
			}
			else {
				// @todo Fetch user details from DB
				$db = new DatabaseUser;
				$options = $db->fetchUser($id);				
				@self::$_instance = new self($options);
			}
		}

		return self::$_instance;
	}

	/**
	 * Fetches user details from database
	 */
	protected static function findUserById($id, Database $db = null)
	{
		 $result = $this->_db->fetchUserById($id);
		 return $result;
	}
	
	
	
	
	
	private function _fetchUserCityName($cityId)
	{		
			
			$this->cityName = $this->_UserDb->getCityNameByCityId($cityId);//get city Name
	}
	
	private function _fetchStateName($stateId)
	{
			
			$this->stateName = $this->_UserDb->getStateNameByStateId($stateId);//get State Name
	}
	
	private function _fetchUserCountryName($countryId)
	{		
			
			$this->countryName = $this->_UserDb->getCountryNameByCountryId($countryId);//get country  Name
	}
	
	private function  _arrangeUrls($options)
	{
		
		$urls = json_decode($_SESSION["urls"], true);//gets url from session
		$this->avatar = isset(json_decode($options['Avatar'])->profilePhoto) ?
		$urls["AvatarUrl"].json_decode($options['Avatar'])->profilePhoto : NULL;
		
		$coverPhotoArray = array("http://104.236.60.131/cuploader/public/uploads/images/cover1.jpg", "http://104.236.60.131/cuploader/public/uploads/images/cover2.jpg", "http://104.236.60.131/cuploader/public/uploads/images/cover3.jpg", "http://104.236.60.131/cuploader/public/uploads/images/cover4.jpg", "http://104.236.60.131/cuploader/public/uploads/images/cover5.jpg", "http://104.236.60.131/cuploader/public/uploads/images/cover6.jpg");
		
		$randomImageIndex = rand(0,5);
		$this->coverPhoto =  $coverPhotoArray[$randomImageIndex];
		
		//$this->coverPhoto = isset(json_decode($options['Avatar'])->coverPhoto) ?
		// $urls["CoverPhotoUrl"].json_decode($options['Avatar'])->coverPhoto : NULL;
        
		 
		 
		$this->profileVideo = isset(json_decode($options['Avatar'])->profileVideo)?
		$urls["VideoUrl"]. json_decode($options['Avatar'])->profileVideo : NULL;
		
	}
	
	
}
