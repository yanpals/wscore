<?php
/**
 *
 */
namespace Mf_Core;

/**
 * Framework
 */
class Framework
{
	const VERSION = "1";
	const LICENCE = "GNU (GPL) Licence";
	const COPYRIGHT = "(c) 2015 Christer Emmanuel. All rights reserved";
}