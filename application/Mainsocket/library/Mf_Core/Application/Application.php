<?php
/**
 *
 */
namespace Mf_Core\Application;

use \Exception;

/**
 * Mf_Core\Application\Abstract
 *
 * The FrontController.
 *
 * This script thandles all are common to the
 * application or the framework, such as:
 * - Session handling
 * - Caching, 
 * - Input filtering.
 * Based on the specific request, it would then instantiate
 * further objects and call methods to handle the particular
 * task(s) required.
 * @see http://en.wikipedia.org/wiki/Front_Controller_(Design_Pattern)
 *
 * It also:
 * - Catches all exceptions that bubbles up to the application level.
 * 	and displays more useful information to the application user.
 *	Exceptions include: 404Exception, 500Exception
 * - It logs the exceptions
 *
 * PROCEDURES:
 * ===============================================
 * Step 1: Application Starts // $app->initialise()
 * ===============================================
 * - Load Config
 * - Load Registry
 *
 * Optional:
 * - Session is started
 * - User is created
 * 
 * =================================================
 * Step 2: Application is Executes // $app->execute()
 * =================================================
 * - //FrontController is initilised
 * - Request object is created
 * - Routing done
 * - Component is executed/Controller is executed
 *       - Perform session check
 *       - Perform access check
 *       - CSRF::checkToken() Macaddress,
 *       - Check user
 *       - Load template
 *       - Return response
 *
 * ===================================================
 * Step 3: Response is received // $app->render()
 * ===================================================
 * - Response is initialised
 * - response is sent.
 *
 * @package 	Mule
 */
abstract class Application
{
	/**
	 * Absolute root path
	 *
	 * @var String
	 */
	protected $_path;

	/**
	 * @var \Mule\Application
	 */
	protected static $_instance;
	
	/**
	 * @param 	String 	$path	Base path to the application
	 */
	protected function __construct($path)
	{
		if(version_compare(PHP_VERSION, '5.4', '<'))
		{
			throw new Exception('UPGRADE TO HIGER VERSION OF PHP > 5.4', 500);
		}

		$this->_path = realpath($path);
	}

	/**
	 * Creates and returns Application instance
	 */
	public static function getInstance($path = '', $namespace = '', $client = 'Web')
	{
		if(self::$_instance)
		{
			return self::$_instance;
		}

		$clientClass = __NAMESPACE__ . '\\' . ucfirst($client);
		self::$_instance = $clientClass::getInstance($path, $namespace);

		return self::$_instance;
	}

	/**
	 * Returns absolute (full) application path
	 *
	 * @return 	string
	 */
	public function getPath()
	{
		return $this->_path;
	}

	/**
	 * Exits the application
	 */
	public function quit($code=0)
	{
		exit($code);
	}

    /**
     * Server api
     */
    public function getSapi()
    {
    	$sapi;

    	if(function_exists('php_sapi_name'))
    	{
    		$sapi = php_sapi_name();
    	}
    	else
    	{
    		$sapi = PHP_SAPI;	
    	}

    	return $sapi;
    }
	
	// --------------------------------------------
	// Initialise Application. Application is started.
	// -----------------------------------------
	// - Load and parse configuration. Throw exception if
	// there are errors in the config file
	// - Session is started
	// - User is created
	// ----------------------------------------------
	abstract public function initialise();

	// ---------------------------------------
	// Dispatch Application. Application is dispatched
	// ---------------------------
	// - Request is received
	// - Router parses request
	// - Component or controller is executed
	//		- If requested component/controller is not found,
	//		ErrorController or ErrorComponent is executed.
	// - Component loads layout template using the View and assigns data
	//   required by the view.
	//
	// -------------------------------------
	// Render response. Response is received.
	// -------------------------------------
	// - Response is initialised
	// - Response is sent
	// ------------------------------------
	abstract public function execute();
}
