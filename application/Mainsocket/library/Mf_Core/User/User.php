<?php
/**
 *
 */
namespace Mf_Core\User;

use \Exception;

/**
 * Represents the user interacting with the
 * application.
 *
 * This API can be extended to add functionality
 * and properties required on client applications
 * including ACL, username and password, etc.
 *
 * @package 	Mule
 */
abstract class User
{
	/**
	 * User id
	 *
	 * @var 	int 	0 represents Guest.
	 */
	protected $_id;

	/**
	 *
	 */
	protected static $_instance;

	/**
	 *
	 */
	public function __construct(array $options = array())
	{
		$this->_id = isset($options['id']) ? $options['id'] : 0;
	}

	/**
	 * Singleton
	 */
	public static function getInstance($id = 0)
	{
		throw new Exception('EXCEPTION_USER_NOT_FOUND', 500);
	}
}