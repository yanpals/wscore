<?php
/**
 *
 */
namespace Mf_Core;

/**
 * 
 */
abstract class Autoloader
{
    public static function register()
    {
        spl_autoload_register(array(__NAMESPACE__ . "\\Autoloader", "autoload"));
    }

	/**
	 * Autoloads a class
	 */
	public static function autoload($className)
	{
        //
        // Mf_Core\Application
        // Mf_Core\Http\Response
        //
        $temp = explode('\\', trim($className));
        $className = array_pop($temp);
        $fileName = __DIR__ . '/../' . implode(DIRECTORY_SEPARATOR, $temp);
        $fileName .= DIRECTORY_SEPARATOR . $className . '.php';

        // Require file only if it exists.
        // Else let other registered autoloaders worry about it.
        if (file_exists($fileName)) {
            require $fileName;
        }
	}
}