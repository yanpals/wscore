<?php
/**
 *
 */
namespace Mf_Core\Auth;

/**
 * Authentication Handler
 */
abstract class Handler
{
	/**
	 *
	 */
	protected $_options;
	
	/**
	 * Url to redirect to on succesful authentication
	 */
	protected $_successUrl;
	
	/**
	 * Url to perform authenticate
	 * e.g Login page
	 */
	protected $_authUrl;
	
	/**
	 * Creates Auth Handler
	 */
	public function __construct(array $options = array())
	{
		$this->_options = $options;
	}
	
	/**
	 * Performs authentication
	 */
	abstract public function authenticate(array $credentials = array());

	/**
	 * Destroys previous authentication tokens
	 * and maybe redirects application to $authUrl
	 */
	abstract public function destroy();
	
	/**
	 * Validates authentication.
	 * The purpose is to ensure that object trying to access a secured area
	 * was authenticated.
	 * 
	 * Checks Token:
	 * Matches Request submitted token with internal token.
	 * This aims to prevent CSRF
	 */
	abstract public function validate();
	
	/**
	 * Creates an Auth Token to identify object
	 */
	abstract protected function _createToken();
	
	/**
	 * Checks for Auth Token
	 */
	abstract protected function _checkToken();
	
	/**
	 * Get AuthToken
	 */
	abstract public function getToken();
	
	/**
	 * Example Url after successful login
	 */
	public function setSuccessUrl($url)
	{
		if(!filter_var($url, FILTER_VALIDATE_URL)) {
			trigger_error("", E_USER_ERROR);
		}
		$this->_successUrl = $url;
	}
	
	/**
	 * E.g Url to login page
	 */
	public function setAuthUrl($url)
	{
		if(!filter_var($url, FILTER_VALIDATE_URL)) {
			trigger_error("", E_USER_ERROR);
		}
		$this->_authUrl = $url;
	}
	
	/**
	 * Redirects application to success url
	 */
	public function redirectSuccessUrl()
	{
		if(!$this->_successUrl)
		{
			return false;
		}
		header("location: " . $this->_successUrl, false, 307);
		exit;
	}
	
	/**
	 * Redirects application to auth url
	 */
	public function redirectAuthUrl()
	{
		if(!$this->_authUrl)
		{
			return false;
		}
		header("location: " . $this->_authUrl, false, 307);
		exit;
	}
}


