<?php
/**
 *
 */
namespace Mf_Core;

use Reflection;
use ReflectionClass;

/**
 * Show information about a class
 * with api documentations in the sourcecode
 *
 * - Code documentations
 * - Constants
 * - Static Properties
 * - Static Methods
 * - Properties
 * - Methods
 * - Method Arguments
 *
 * Purpose:
 * This api will enable users of these api view what
 * is contained within a class or object, temporarily
 * until a full documentation is developed.
 */
class Reflector
{
	public static function reflect($object)
	{
		echo "<pre>";
		Reflection::export( new ReflectionClass($object) );
		exit;
		echo "</pre>";
	}
}
