<?php
/**
 *
 */
namespace Mf_Core;

/**
 * Cookie
 *
 * @See PHP Manual:
 * - setcookie
 */
class Cookie
{
	//
	protected static $_instance;
	
	//
	protected function __construct(array $options = array())
	{
		// Thinking...
	}
	
	//
	public static function getInstance()
	{
		if(!self::$_instance)
		{
			self::$_instance = new self;
		}
		
		return self::$_instance;
	}
	
	// @param 	string 	$name
	// @param 	mixed 	$value		Default cookie supports only strings. You can now store objects, array, etc.
	// @param 	int 	$expire 	The number of seconds after which cookie will expire
	// @param 	string 	$path
	// @param 	string 	$domain
	// @param 	bool 	$secure
	// @param 	bool 	$httponly	Prevents cookie from being access via client-side scripts
	//
	// You can use strtotime('+30 days') to set expiry or time()+60*60secs
	public static function set($name, $value, $expire=0, $path='/', $domain='', $secure=false, $httponly=false)
	{
		// Objects and arrays needs to be serialized.
		// JSON encoding is used instead of serialize()
		// so data can be accessed on client.
		if(is_object($value) || is_array($value))
		{
			$value = json_encode($value);
		}
		
		return setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
	}
	
	// @param 	string 	$name
	// @param 	bool 	$serialized		Set to true if you expect object or array
	public static function get($name, $serialized = false)
	{
		$value = null;
		if(isset($_COOKIE[$name]))
		{
			$value = $_COOKIE[$name];
		}
		
		// Need to decode value
		if($value && $serialized) {
			$value = json_decode($value);
		}
		
		return $value;
	}
	
	// Delete cookie
	public static function remove($name)
	{
		if(isset($_COOKIE[$name]))
		{
			// Ensure that cookie is deleted on client
			setcookie($name, '', time()-3600);
			
			// Remove completely
			unset($_COOKIE[$name]);
		}
	}
}

/**
Cookie::set('users', array('christer', 'annie'), time() + 60*60*24*30);

var_dump($_COOKIE);
var_dump(Cookie::get('users', true));
// Cookie::remove('user');

// Get cookie from javascript
var cookie = document.cookie
, users = decodeURIComponent( cookie.split('=')[1] );
JSON.parse( users );
*/
