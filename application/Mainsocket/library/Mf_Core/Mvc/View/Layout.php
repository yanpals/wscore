<?php
/**
 *
 */
namespace Mf_Core\Mvc\View;

use \Exception;

/**
 * View Layout
 *
 * Most times, contents returned from the component
 * has to be wrapped in a custom layout.
 * This class provides a wrapper view for this purpose.
 */
class Layout
{
	// layout path
	protected $_path;
	
	// Theme options e.g name, author, etc
	protected $_options;
	
	// Enabled modules
	protected $_modules;
	
	//
	protected $_styleRules;
	
	//
	protected $_styleLinks;
	
	// <script src=""...
	protected $_scriptSrc;
	
	// Link tags <link href="" rel="search"...
	protected $_metaLinks;
	
	// <meta http-equiv=''... || <meta name=''...
	protected $_metaTags;

	// Theme vars are dynmically assigned content
	protected $_vars;
	
	//
	protected static $_instance;
	
	// @param 	string 	$path	Directory to layout files
	// @param
	protected function __construct($path, array $options = array())
	{
		$initfile = $path . DIRECTORY_SEPARATOR . 'layout.php';
		if(!file_exists($initfile))
		{
			throw new Exception('EXCEPTION_LAYOUT_NOT_FOUND', 500);
		}

		$this->_path = $path;
		$this->_options = $options;
		
		// Load initfile for layout
		ob_start();
		require_once $initfile;
		$buffer = ob_get_contents();
		ob_end_clean();
	}
	
	//
	public static function getInstance($path = null, array $options = array())
	{
		if(!self::$_instance)
		{
			// $class = __NAMESPACE__ . "\\Layout\\" . ucfirst($format);
			// self::$_instance = new $class($path, $options);
			self::$_instance = new self($path, $options);
		}
		
		return self::$_instance;
	}
	
	/**
	 * Merges old theme options with new options
	 */
	public function setOptions(array $options)
	{
		$this->_options = array_merge($this->_options, $options);
	}
	
	/**
	 * Get theme option
	 */
	public function getOption($key)
	{
		if(isset($this->_options[$key]))
		{
			return $this->_options[$key];
		}
		
		return '';
	}
	
	/**
	 *
	 */
	public function setOption($key, $value)
	{
		$this->_options[$key] = $value;
	}
	
	/**
	 * Set layout var
	 */
	public function set($var, $value)
	{
		$this->_vars[$var] = $value;
	}
	
	/**
	 * Get a layout var
	 */
	public function get($var, $default = '')
	{
		if(isset($this->_vars[$var]))
		{
			return $this->_vars[$var];
		}
		
		return $default;
	}
	
	// Loads a style declaration
	public function loadStyleRule($name, $style)
	{
		// Init $this->_styleRules
		if(!is_array($this->_styleRules)) {
			$this->_styleRules  = array();
		}
		$this->_styleRules[$name] = $style;
	}
	
	// @param 	$url 	string		Url to stylesheet
	// @param 	$defer 	bool		Force layout to load style at bottom of the tmpl
	public function loadStyleLink($url, $defer = false)
	{
		//...
	}
	
	// Loads layout module
	//
	// @param 	$path 			string 	Path to module
	// @param 	$modContent 	string	Module content can be set through argument
	public function loadModule($name, $modContent = '')
	{
		// Init $this->_modules
		if(!is_array($this->_modules)) {
			$this->_modules  = array();
		}
		
		if(empty($modContent))
		{
			$modFilePath = $this->_path . '/modules/mod_' . $name . '.php';
			if(!file_exists($modFilePath))
			{
				throw new Exception("EXCEPTION_LAYOUT_MODULE_NOT_FOUND", 500);
			}
			
			ob_start();
			require_once $modFilePath;
			$modContent = ob_get_contents();
			ob_end_clean();	
		}
	
		$this->_modules[$name] = $modContent;
	}
	
	// Loads and parse the layout file
	protected function _loadTmpl($tmpl)
	{
		$tmpl = $this->_path . '/' . $tmpl . '.tmpl.php';
		
		if(!file_exists($tmpl)) {
			throw new Exception('EXCEPTION_LAYOUT_TMPL_NOT_FOUND', 500);
		}
		
		// Extract vars to local variables
		if(is_array($this->_vars) && !empty($this->_vars)) {
			extract($this->_vars);
		}
		
		// Extract modules to local variables referenced with 'mod_' . $modName
		if(is_array($this->_modules) && !empty($this->_modules)) {
			extract($this->_modules, EXTR_OVERWRITE | EXTR_PREFIX_ALL, 'mod');
		}
		
		ob_start();
		require_once $tmpl;
		$content = ob_get_contents();
		ob_end_clean();
		
		return $content;
	}
	
	/**
	 *
	 */
	public function render($tmpl = 'default', $return = true)
	{
		if($return)
		{
			return $this->_loadTmpl($tmpl);
		}
		
		echo $this->_loadTmpl($tmpl);
		return true;
	}
}

/*
$layout = Layout::getInstance('/Htpro/www/__MULE/FRAMEWORK/public/assets');
$layout->loadModule('content', 'Hello world');
$layout->render('default', false);

var_dump( $layout );
*/