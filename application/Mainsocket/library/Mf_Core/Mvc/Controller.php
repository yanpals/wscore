<?php
/**
 *
 */
namespace Mf_Core\Mvc;

use \Exception;
use \ReflectionClass;
use \Mf_Core\Application\Application;
use \Mf_Core\Mvc\View;
use \Mf_Core\Mvc\Model;

/**
 * Mf_Core\Controller
 *
 * Only one (1) controller is loaded for a single request.
 * It returns response wrapped in single view
 *
 * What it does?
 * - Provides methods to set state in Model
 * - Provides methods to refresh view
 * - Creates and load its Model
 * - Creates and load its View
 *
 * @package
 */
abstract class Controller
{
	/**
	 * Name of the controller
	 */
	protected $_name;

	/**
	 *
	 */
	protected $_path;

	/**
	 *
	 */
	protected $_view;

	/**
	 *
	 */
	protected $_model;
	
	/**
	 * Singleton instance of controller
	 */
	protected static $_instance;

	/**
	 * Constructs Mf_Core\Controller
	 */
	protected function __construct(View $view = null, Model $model = null)
	{
		// Dynamically find controller path & name
		$r = new ReflectionClass($this);
		$this->_path = dirname($r->getFileName());
		$this->_name = $r->getName();

		// Set dependencies
		$this->_view = $view;
		$this->_model = $model;
	}
	
	/**
	 * Creates instance of a controller
	 *
	 * @param 	string 	$name 		Controller name
	 * @param 	string 	$action 	Action
	 * @param 	string 	$path 		Application path
	 */
	public static function getInstance($name="", $action="")
	{
		if(is_object(self::$_instance)) {
			return self::$_instance;
		}

		// Create controller path
		$application = Application::getInstance();
		$path = $application->getPath() . '/application/controllers/' .
			strtolower($name) . '/' . strtolower($action) . '.php';
		if(file_exists($path)) {
			require_once $path;
		}

		// Create class
		$class = ucfirst($name) . 'Controller' . ucfirst($action);
		if(!class_exists($class))
		{
			throw new Exception("EXCEPTION_CONTROLLER_NOT_FOUND", 404);
		}

		self::$_instance = new $class;
		return self::$_instance;
	}

	/**
	 * All child controllers must override the execute method
	 *//*
	abstract public function execute();
	*/

	/**
	 *
	 */
	public function getName()
	{
		return $this->_name;
	}

	/**
	 * Method to find view for this controller. Creates View
	 * if not already created
	 *
	 * View path looks similar to:
	 * application/views/controller_name/controller_action_name/view.$format.php
	 */
	public function getView($format = 'html')
	{
		if(is_object($this->_view))
		{
			return $this->_view;
		}

		// Create View
		$application = Application::getInstance();
		preg_match('/^(.*)Controller(.*)$/i', $this->getName(), $m);
		$path = $application->getPath() . '/application/views/' .
			strtolower($m[1]) . '/' . strtolower($m[2]) . '/view.' . $format . '.php';
		$class = $m[1] . 'View' . $m[2];

		if(!class_exists($class))
		{
			if(!file_exists($path))
			{
				throw new Exception("EXCEPTION_VIEW_NOT_FOUND", 404);
			}

			require_once $path;
			if(!class_exists($class))
			{
				throw new Exception("EXCEPTION_VIEW_NOT_FOUND", 404);
			}
		}

		$this->_view = new $class;
		return $this->_view;
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * View path looks similar to:
	 * application/models/controller_name/controller_action_name/view.$format.php
	 */
	public function getModel()
	{
		if(is_object($this->_model))
		{
			return $this->_model;
		}

		// Create Model
		$application = Application::getInstance();
		preg_match('/^(.*)Controller(.*)$/i', $this->getName(), $r);
		$path = $application->getPath() . '/application/models/' .
			strtolower($r[1]) . '/' . strtolower($r[2]) . '.php';
		$class = $r[1] . 'Model' . $r[2];

		if(!class_exists($class))
		{
			if(!file_exists($path))
			{
				throw new Exception("EXCEPTION_MODEL_NOT_FOUND", 404);
			}

			require_once $path;
			if(!class_exists($class))
			{
				throw new Exception("EXCEPTION_MODEL_NOT_FOUND", 404);
			}
		}

		$this->_model = new $class;
		return $this->_model;
	}

	/**
	 *
	 */
	protected function display($tmpl = 'default')
	{
		$this->getView()->display($tmpl);
	}
}
