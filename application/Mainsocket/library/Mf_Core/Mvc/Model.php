<?php
/**
 *
 */
namespace Mf_Core\Mvc;

use \ReflectionClass;
use Mf_Core\Database\Database;

/**
 * MVC Model
 *
 * Model keeps states and communicates with the
 * database or DOMAIN MODELS.
 *
 * @package
 */
abstract class Model
{
	/**
	 *
	 */
	protected $_name;

	/**
	 *
	 */
	protected $_path;

	/**
	 * @var 	Database
	 */
	protected $_db;
	
	/**
	 *
	 */
	public function __construct(Database $db = null)
	{
		// Dynamically find controller path & name
		$r = new ReflectionClass($this);
		$this->_path = dirname($r->getFileName());
		$this->_name = $r->getName();

		// Set db
		$this->_db = $db;
	}

	/**
	 * Method to create and return an MVC Model
	 */
	public static function getModel($name)
	{
		//
	}

	/**
	 * @param 	Database 	$db
	 * @return 	void
	 */
	public function setDb(Database $db)
	{
		$this->_db = $db;
	}

	/**
	 * @param 	void
	 * @return 	Database
	 */
	public function getDb()
	{
		return ($this->_db) ? $this->_db : null;
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->_name;
	}

	/**
	 * Getter method for getting the property (or state)
	 * of an Object
	 */
	public function get($property)
	{
		// @todo
		throw new Exception("This method has not been implemented", 500);
	}

	/**
	 * Setter method for setting a Model's property
	 * or state
	 */
	public function set($property, $value)
	{
		// @todo
		throw new Exception("This method has not been implemented", 500);
	}
}

