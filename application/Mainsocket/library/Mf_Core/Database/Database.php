<?php
/**
 *
 */
namespace Mf_Core\Database;

use Mf_Core\Database\Driver\DriverInterface;

/**
 * Database wrapper for maintaining
 * database queries.
 *
 * The purpose of this class is to maintain all interactions with a database
 * in  one class.
 *
 * Call the driver within the class to perform queries.
 */
class Database
{
	/**
	 * @var 	DriverInterface
	 */
	protected $_driver;

	/**
	 * @var 	string 	Name of database
	 */
	protected $_name;

	/**
	 * @var 	string 	Default charset
	 */
	protected $_charset = 'UTF-8';

	/**
	 * Database instances
	 */
	protected static $_instances = array();

	/**
	 *
	 */
	public function __construct($name, DriverInterface $driver)
	{
		$this->_name = $name;

		$this->_driver = $driver;
		$this->_driver->setDatabase($this->_name);

		// @todo Set charset
	}

	/**
	 * Returns single instance of database
	 * Multiple database connection is supported.
	 */
	public static function getInstance($name, DriverInterface $driver)
	{
		$key = md5($name . $driver);
		if(!isset(self::$_instances[$key]))
		{
			self::$_instances[$key] = new self($name, $driver);
		}

		return self::$_instances[$key];
	}

	/**
	 *
	 */
	public function __clone()
	{
		// throw new Exception
	}

	/**
	 *
	 */
	public function getDriver()
	{
		return $this->_driver;
	}

	/**
	 *
	 */
	public function setDriver(DriverInterface $driver)
	{
		$this->_driver = $driver;
	}

	/**
	 * Create a database driver
	 */
	public static function createDriver($type = 'pdo', array $options)
	{
		$class = __NAMESPACE__ . '\\Database\\Driver\\' . $type;

		$host = isset($options['host']) ? $options['host'] : '127.0.0.1';
		$user = isset($options['user']) ? $options['user'] : 'root';
		$passwd = isset($options['passwd']) ? $options['passwd'] : '';
		$dbname = isset($options['dbname']) ? $options['dbname'] : '';

		$driver = new $class($host, $user, $passwd, $dbname);
		return $driver;
	}

	/**
	 * Sample query method
	 */
	protected function _query()
	{
		return call_user_func_array($this->_driver->query, func_get_args());
	}

	/**
	 *
	 */
	public function __destroy()
	{
		// $this->_driver->close();
	}
}