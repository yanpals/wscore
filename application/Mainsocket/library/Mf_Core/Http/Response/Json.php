<?php
/**
 *
 */
namespace Mf_Core\Http\Response;

/**
 * Standard json response object.
 * This is used to maintain a consistent json response
 * within your application.
 *
 * {error:null,payload:{},ts:}
 * {success:1,payload:{ui:""},ts:}
 */
class Json
{
	// Tell whether a request is successful
	public $success;
	
	// Payload contains the actual data returned or null
	// if nothing is there to return
	// @var 	mixed 	null on empty, object or array
	public $payload;
	
	// Response time
	public $timestamp;
	
	/**
	 *
	 */
	public function __construct($success, $payload = null)
	{
		$this->success = $success;
		$this->payload = $payload;
		$this->timestamp = time();
	}

	/**
	 * Magic toString method for sending the response in JSON format
	 *
	 * @return  string  The response in JSON format
	 *
	 * @since   3.1
	 */
	public function __toString()
	{
		return json_encode($this);
	}
}
