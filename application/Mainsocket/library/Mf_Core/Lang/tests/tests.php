<?php

require_once '../TypeAbstract.php';
require_once '../_Array.php';
require_once '../_Object.php';
require_once '../_Boolean.php';
require_once '../_Integer.php';

use Mf_Core\Lang\_Array;
use Mf_Core\Lang\_Object;
use Mf_Core\Lang\_Boolean;
use Mf_Core\Lang\_Integer;

//
// _Array
//

//$arr = new _Array('Angola', true, 34); // Scalar members
$arr = new _Array(array('Nigeria', 'Cameroun'), array('Bakassi', 'Calabar')); // Multi-dim array
//$arr = new _Array(new _Array('Nigeria', 'Cameroun'), new _Array('Bakassi', 'Calabar')); // Multi-dim array
//$arr = new _Array(new Book()); // Type members
var_dump($arr->getValue());

//var_dump($arr->each());
//var_dump($arr->merge(new _Array('a', 'b', 'c')));
var_dump("Array Size: " . $arr->getCount());
var_dump("Array JSON: " . $arr->toJsonString());

//
// _Object
//

$obj = new _Object(array('a'=>"Ama", 'b'));
var_dump( (string) !!$obj->getValue() );
$obj->setProperty('a', 'Christer');
print_r( $obj->getValue() );
print_r( $obj->getProperty('0') );
var_dump("Object JSON: " . $obj->toJsonString());

//
// Boolean
//

/*
$bool = new _Boolean(1);
var_dump( $bool->toString() );
*/

//
// Integer
//

$int = new _Integer('0xff', 16);
//var_dump($int->getValue());
// var_dump(sprintf('%d', '0xff')); // 0
// var_dump(sprintf('%d', 0xff)); // 255
var_dump($int->getBin());
