<?php
/**
 * Main WebSocket wsengine Controller
 */
namespace Mf\Mainsocket;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\User;
use Mf\Mainsocket\CommentWriter;
use Mf\Mainsocket\RateWriter;
use Mf\Mainsocket\CommentDeleter;
use Mf\Mainsocket\EditPost;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;
use Mf\Mainsocket\library\Mainsocket\UserHelper;
session_start();

/**
 *
 *

 */
class MainSocket extends \Mf\WebSocket\Event\Observer
{
	// Main database object
	protected $_dbMain;
	protected $_UserDb;
	protected $_NotificationDb;


	// Invidual users
	protected $_users = array();


	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->_dbMain = Registry::getInstance()->get('MainDb');
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_NotificationDb = Registry::getInstance()->get('NotificationDb');
		if(!isset($_SESSION["urls"])){//if url is not in session
		$this->fetchUrls();
		}
	}

	/**

	 * - Send session id to client/browser (if not created)
	 * - Fetch data for current session, if it has not expired (time after last

	 */
	public function onConnect(Event $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$ip = $client->ip;

		$msg = "Client $ip has connected";
		$server->log($msg);
	}

	/**
	 * - Remove user from chat
	 * - Log disconnected user
	 */
	public function onDisconnect(Event $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$socket = $client->socket;
		$ip = $client->ip;

		$msg = "Client $ip has disconnected";
		$server->log($msg);

		// Remove disconnected user by socket
		foreach($this->_users as $user)
		{
			if($socket === $user->socket)
			{
				$this->onUserLeave($user, $event);
				break;
			}
		}
	}

	/**
	 * - Check that it's supported message
	 * - Notify connected users
	 * - Save message in database
	 */
	public function onMessage(Event $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$message = json_decode($event->getParam('message'));


		if(!$message) {
			var_dump('Message Error');
			//$server->log(json_encode($msg));
			return false;
		}

		/*if($message->controller !== 'comment' && $message->controller !== 'rate' && $message->controller !== 'post'){
			return false;
		}*/

		if($message->action === 'user_join'){
			$this->onUserJoin($message, $event);
		}



		if($message->action === 'writecomment'){
		$CommentWriter = new CommentWriter($message, $event);
		$CommentWriter->AddEventListerner('onComment', $CommentWriter, 'notifyPostOwner');
		$CommentWriter->AddEventListerner('onComment', $CommentWriter, 'notifyOtherCommenters');
		$CommentWriter-> execute($message, $event);
		$returnedData = $CommentWriter->build();
		$this->serveComment($returnedData, $event);
		}

		if($message->action === 'deletecomment'){
		$CommentDeleter = new CommentDeleter($message, $event);
		$returnedData = $CommentDeleter->build();
		$this->serveCommentDeleter($returnedData, $event);
		}




		if($message->action === 'editpost'){
			$EditPost = new EditPost($message, $event);
			$returnedData = $EditPost->build();
			$this->GeneralResponse($returnedData, $event);//sends the data to those who are concerned and online ;
		}

	}

	public function onUserJoin($message, $event)
	{
		$user = $message->user;
		$server = $event->getTarget();
		$client = $event->getParam('client');

		if(!isset($user->id)) return false;

		// fetch user details
		$properties = $this->_UserDb->fetchUser($user->id);
		if(!is_array($properties)) {
			//@Error:
			$msg = json_encode(array(
				'controller' => 'comment',
				'action' => 'error',
				'message' => 'User does not exist'
			));
			$server->send($client->socket, $msg);
			$server->log($msg);
			return false;
		}
		$newUser = new UserHelper($user->id, $properties);
		$newUser->socket = $client->socket;
		$newUser->ip = $client->ip;
		$this->addUser($newUser);

	}




	/**
	 * - Broadcast user that has left chat
	 * - Remove use from users
	 */
	public function onUserLeave($user, $event)
	{
		$userIndex = -1;
		foreach($this->_users as $index => $tmpUser)
		{
			if($user == $tmpUser) $userIndex = $index;
		}

		if($user == false || $userIndex == -1) return false;

		$server = $event->getTarget();
		$client = $event->getParam('client');

		// Unset socket, ip
		unset($user->ip, $user->socket);

		// Prepare message
		$msg = json_encode(array(
			'controller' => 'comment',
			'action' => 'user_leave',
			'user' => $user
		));

		// Remove user
		if($this->removeUser($userIndex))
		{
			// Broadcast disconnected user to all other connected users
			foreach($this->_users as $recvr)
			{
				if($user !== $recvr || !!$recvr->socket)
					$server->send($recvr->socket, $msg);
			}
		} else {
			return false;
		}

		return true;
	}





	/**
	 * Adds user to chat
	 */
	public function addUser($user)
	{
		array_push($this->_users, $user);
	}

	/**
	 * Removes a user from chat
	 */
	public function removeUser($index)
	{
		if($index > -1 && $index < count($this->_users))
		{
			array_splice($this->_users, $index, 1);
			return true;
		}

		return false;
	}

	/**
	 *
	 */
	public function getUserById($id)
	{
		foreach($this->_users as $user)
		{
			if($user->UserId === $id)
			{
				return $user;
			}
		}

		return null;
	}


	public function fetchUrls()
	{

		$url = $this->_UserDb->fetchUrls();
		$_SESSION["urls"] =  json_encode($url);
	}


	 public function serveComment($returnedData, $event)
	 {
		$server = $event->getTarget();
		$client = $event->getParam('client');


		 $UserPalsId = $returnedData["usr"];
			$msg = $returnedData["msg"];
			$msg["notify"] = "false";
			$msg["profiling"] = (microtime(true) - $returnedData["startTime"]);
			 $msg = json_encode($msg);

			$server->send($client->socket, $msg);

			foreach($UserPalsId as $one){

				foreach($this->_users as $use){
					if($use->UserId == $one){
						if(in_array($one, $returnedData["notReciever"])){//if among those to be notified instanlty
							$msg2 = $returnedData["msg"];
							$msg2["notify"] = "true";//set it to true
							$msg2["notificationcount"] = $this->_NotificationDb->countUserUnseenNotifications($use->UserId);
							$msg2 = json_encode($msg2);
							$server->send($use->socket, $msg2);
						}
						else{
							$msg2 = $returnedData["msg"];
							$msg2["notify"] = "false";
							$msg2 = json_encode($msg2);
							$server->send($use->socket, $msg2);

						}
					}
				}//ends second foreach
		}//ends first foreach

	 }


	 public function serveCommentDeleter($returnedData, $event)
	 {
		$server = $event->getTarget();
		$client = $event->getParam('client');

		$UserPalsId = $returnedData["usr"];
		$msg = json_encode($returnedData["msg"]);
		$server->send($client->socket, $msg);

			foreach($UserPalsId as $one){

				foreach($this->_users as $use){
					if($use->UserId == $one){
						$server->send($use->socket, $msg);
					}
				}//ends second foreach
		}//ends first foreach

	 }


	 public function GeneralResponse($returnedData, $event)//sends the data to those who are concerned and online ;
	 {
	 $server = $event->getTarget();
		$client = $event->getParam('client');

		$UserPalsId = $returnedData["usr"];
		$msg = json_encode($returnedData["msg"]);
		$server->send($client->socket, $msg);

			foreach($UserPalsId as $one){

				foreach($this->_users as $use){
					if($use->UserId == $one){
						$server->send($use->socket, $msg);
					}
				}//ends second foreach
		}//ends first foreach
	 }//end of function

}//end of class
