<?php
namespace Mf\Mainsocket;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\library\Mainsocket\GeneralFunctions;
use Mf\Mainsocket\library\Mainsocket\Rate;
use Mf\Mainsocket\library\Mainsocket\RateList;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;





Class RateWriter {
	
	protected $event;
	protected $message;
	protected $_UserDb;
	protected $_NotificationDb;
	protected $_PostDb;
	protected $_RateDb;
	protected $_LoggerDb;
	private $_limit = 6;
	public $returnedData = array();
	public $instantNotificationRecievers = array();//these are the people that who are legible to recieve notifications
	
	public function __construct($message, $event)
	{
	
		$this->message = $message;
		$this->event = $event;
		
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_NotificationDb = Registry::getInstance()->get('NotificationDb');
		$this->_PostDb = Registry::getInstance()->get('PostDb');
		$this->_RateDb = Registry::getInstance()->get('RateDb');
		$this->_LoggerDb = Registry::getInstance()->get('LoggerDb');
		$this->prepareData($message, $event);
		
	}
	
	

	
	
	public function prepareData($message, $event)
	{
		$startTime = time();
		$server = $event->getTarget();
		$client = $event->getParam('client');
		
		$postId = $message->postId;
		$userId = $message->userId;
		$ratelevel = $message->level;
		
		$gF = new GeneralFunctions();
		
		$rateId = $gF->IdGenerator();//generate id
		
		
		$resp = $this->_RateDb->write($rateId, $userId, $postId,  $ratelevel, time());

				
				if($resp != "failed"){
				
					
						$threadId = $gF->IdGenerator();
						$threadId2 = $gF->IdGenerator()."rnew";
					
						//send to Notification table
					
						$rate =  $this->_RateDb->getRateById($rateId);

						
						$notificationArea = 1; //for board 
						$this->notifyPostOwnerOnrate($rate, $notificationArea);// notifies the owner of the post
						//$dbNotification->notifyOtherRaters($rate, $notificationArea);//notifies other raters of the post
						
						$db = $this->_PostDb;
						$postProperties = $db->getPostById($postId);
						
						//log into userInteraction table
						
		
						$this->_LoggerDb->logUserInteraction(array(
						'userId' => $postProperties["UserId"],
						'viewerId' => $userId, 
						'objectId' => $postId,
						'objectTypeId' => 1,
						'privacy' => $postProperties["Privacy"],
						'interactionType' => '3',
						'interactionLevel' => $ratelevel,
						'date' => date("Y-m-d")
						));
						
						
						$postOwnerUserId =  $postProperties["UserId"];
						
				
				$msg = array("postId"=>$postId, "rateId"=>$rateId, "userId"=>$userId, "rateCount"=>$resp, "controller"=>"rate", "action"=>"writerate");
				
				
				$UserPalsId = $this->fetchUserPals($postOwnerUserId);

		$this->returnedData = array("msg"=>$msg, "usr"=>$UserPalsId, "notReciever"=>$this->instantNotificationRecievers, "startTime" =>$startTime);
			
		}
		
		
	}//ends dunction
	
	
	public function notifyPostOwnerOnrate($rate, $notificationArea)
	{
		$postDb = $this->_PostDb;
		$postOwner = $postDb->getPostOwner($rate["PostId"]);	
		//check if the rater is not the Post owner
		if($postOwner != $rate["UserId"]){
			if(!$this->_NotificationDb->checkStoppedSubscription($postOwner, $rate["PostId"])){//if the user hasn't stopped notification 
				$this->instantNotificationRecievers[] = $postOwner;
				$userDb = $this->_UserDb;
				$postId = $rate["PostId"];
				$userId =  $postOwner;
				$type = 2;
				$dateTimeLogged = time();
				$status = 0;
				$contArray = json_encode(array( "PostId"=>$rate["PostId"], "CauseUserId"=>$rate["UserId"]));//creates an array to hold the content of 					             	 notification, the postId involved ant the userid of the person that made the notification to take place	
				$this->_NotificationDb->deposit($userId, $contArray, $type, $dateTimeLogged, $status, $notificationArea);
			}
		}
		return true;

	}
	
	
	
	
	private function fetchUserPals($userId)
	{
		$pals = $this->_UserDb->getUserPalsSpecial($userId, 5000);//fetchs userid of all the pals
		
		if(is_array($pals) && count($pals) > 0){
		$palsId = array();
		
		foreach($pals as $pal)
		{
			$palsId[] = $pal["UserId"];
			
		}
		return $palsId;
		
		}//ends if
	}
	
	
	private function getUserProperties($onePalId){
		//connect to user database and get UserInfo as Object	
		
		$userInfo = $this->_UserDb->fetchUser($onePalId);
		return $userInfo;
		 
	}
	
	
	
	public function build()
	{
		return $this->returnedData;
	}
	
	
}//ends class



?>